<?php

use App\Aliado;
use Illuminate\Database\Seeder;

class AliadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen(public_path('aliados-datos-inicial.csv'), "r")) !== FALSE) {
            while (($aliado = fgetcsv($handle, 1000, ",")) !== FALSE) {


                Aliado::query()->insert([
                    'nombre'=>$aliado[0],
                    'url'=>$aliado[1],
                ]);
            }
            fclose($handle);
        }
    }
}
