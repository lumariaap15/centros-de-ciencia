<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'admin@gmail.com'
        ]);
        $this->call(DepartamentosSeeder::class);
        $this->call(MunicipiosSeeder::class);
        $this->call(CategoriaCentrosSeeder::class);
        $this->call(CentrosCienciaSeeder::class);
        //factory(\App\Proyecto::class,30)->create();
        $this->call(EjemploCentroSeeder::class);
        $this->call(AliadosSeeder::class);
    }
}
