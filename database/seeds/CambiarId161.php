<?php

use Illuminate\Database\Seeder;

class CambiarId161 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = \App\Centro::query()->findOrFail(161);
        $c->id = 12;
        $c->save();
    }
}
