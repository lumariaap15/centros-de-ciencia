<?php

use App\Centro;
use App\CentrosCategoria;
use App\Departamento;
use App\Municipio;
use Illuminate\Database\Seeder;

class CentrosCienciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen(public_path('base-de-datos-inicial.csv'), "r")) !== FALSE) {
            while (($centro = fgetcsv($handle, 1000, ";")) !== FALSE) {

                $categoria = CentrosCategoria::query()->where('nombre',
                    $centro[9])->first();


                foreach (Municipio::all() as $municipio){
                    $municipioArchivo = mb_strtoupper(str_replace(".", "", str_replace(" ","", $centro[7])));
                    $municipioDB = mb_strtoupper(str_replace(",","",str_replace(".", "", str_replace(" ","", $municipio->descripcion))));
                    if($municipioArchivo === $municipioDB)
                    {
                        break;
                    }
                }

                Centro::query()->insert([
                    'id'=>intval($centro[4]),
                    'nombre'=>$centro[5],
                    'municipio_codigo'=>$municipio->codigo,
                    'departamento_codigo'=>$municipio->departamento_codigo,
                    'direccion'=>$centro[10],
                    'latitud'=>$centro[12],
                    'longitud'=>$centro[13],
                    'categoria_id'=>$categoria->id
                ]);
            }
            fclose($handle);
        }
    }
}
