<?php

use Illuminate\Database\Seeder;

class BorrarProyectosFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proyectosFaker = \App\Proyecto::query()->where('id','<=','30')->get();
        foreach ($proyectosFaker as $p){
            $p->delete();
        }

    }
}
