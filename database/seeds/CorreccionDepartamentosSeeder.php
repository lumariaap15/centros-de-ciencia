<?php

use Illuminate\Database\Seeder;

class CorreccionDepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deptBogota = \App\Departamento::query()->where('codigo',11)->first();
        $deptBogota->descripcion = 'BOGOTÁ D. C.';
        $deptBogota->save();

        /*
        $m8 = \App\Municipio::query()->where('departamento_codigo','08')->first();
        $m8->departamento_codigo = '8';
        $m8->save();


        $d8 = \App\Departamento::query()->where('codigo','08')->get();
        foreach ($d8 as $d){
            $d->codigo = 8;
            $d->save();
        }
        */


        $munBogota = \App\Municipio::query()->where('codigo',11001)->first();
        $munBogota->descripcion = 'BOGOTÁ D. C.';
        $munBogota->save();

    }
}
