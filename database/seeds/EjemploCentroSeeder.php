<?php

use Illuminate\Database\Seeder;

class EjemploCentroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $centro = \App\Centro::query()->findOrFail(119);
        $centro->descripcion = "ViveLab Bogotá es un laboratorio de innovación, creatividad y nuevas tecnologías que fomenta
la creación de soluciones digitales de alto impacto social y económico. Trabaja de la mano del
Gobierno Nacional y Distrital, empresas, academia y ciudadanos para desarrollar plataformas
digitales bajo metodologías participativas. Sus valores destacados son el aprovechamiento de las
tecnologías, el empoderamiento ciudadano, el desarrollo sostenible, la modernización del
Gobierno, las actividades productivas y la gestación de innovaciones sociales.";
        $centro->owner = "Universidad Nacional de Colombia, sede Bogotá";
        $centro->proyectos()->create([
            'nombre'=>'Acompañamiento a emprendedores en ciencia y tecnología',
            'categoria_id'=>$centro->categoria_id,
        ]);
        $centro->proyectos()->create([
            'nombre'=>'Sensibilización y promoción de procesos de ASCTeI de soluciones tecnológicas a
problemas específicos con las personas beneficiarias',
            'categoria_id'=>$centro->categoria_id,
        ]);
        $centro->proyectos()->create([
            'nombre'=>'Creación de soluciones y plataformas tecnológicas a problemas situados',
            'categoria_id'=>$centro->categoria_id,
        ]);
        $centro->proyectos()->create([
            'nombre'=>'Generación de impacto en instituciones y miembros de la sociedad civil, mediante
procesos de creación de soluciones que aplican tecnología',
            'categoria_id'=>$centro->categoria_id,
        ]);
        $centro->horarios()->create([
            'horario'=>'Con cita previa',
        ]);
        $centro->entradas()->create([
            'nombre'=>'General',
            'valor'=>0,
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Diagnóstico de expertos',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Pruebas con usuarios',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Actividades académicas',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Conferencias',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Seminarios',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Cursos',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Asesorías y capacitaciones',
        ]);
        $centro->servicios()->create([
            'descripcion'=>'Desarrollo de plataformas digitales',
        ]);
        $centro->celular = "3005687569";
        $centro->pagina_web = "www.vivelabbogota.com";
        $centro->correo = "vivelab_bog@unal.edu.co";
        $centro->direccion = "Avenida El Dorado, carrera 45 #26-33";
        $centro->fecha_creacion = \Carbon\Carbon::now();
        $centro->save();

    }
}
