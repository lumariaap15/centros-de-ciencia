<?php

use App\CentrosCategoria;
use Illuminate\Database\Seeder;

class CategoriaCentrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //warning: No cambiar de orden
        CentrosCategoria::query()->create([
            'nombre'=>'Bioespacios',
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'Espacios para las ciencias exactas, físicas, sociales y la tecnología',
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'Espacios en construcción ciudadana en CTeI',
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'Espacios Mixtos',
        ]);

        //BIOESPACIOS
        CentrosCategoria::query()->create([
            'nombre'=>'ACUARIO',
            'categoria_id'=>1,
        ]);
        CentrosCategoria::query()->create([
            'nombre'=>'AVIARIO',
            'categoria_id'=>1,
        ]);
        CentrosCategoria::query()->create([
            'nombre'=>'BIOESPACIO',
            'categoria_id'=>1,
        ]);
        CentrosCategoria::query()->create([
            'nombre'=>'BIOPARQUE',
            'categoria_id'=>1,
        ]);
        CentrosCategoria::query()->create([
            'nombre'=>'JARDÍN BOTÁNICO',
            'categoria_id'=>1,
        ]);
        CentrosCategoria::query()->create([
            'nombre'=>'ZOOLÓGICO',
            'categoria_id'=>1,
        ]);

        //2
        CentrosCategoria::query()->create([
            'nombre'=>'MUSEO ARQUEOLÓGICO',
            'categoria_id'=>2,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'MUSEO DE CIENCIA',
            'categoria_id'=>2,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'MUSEO PALEONTOLÓGICO',
            'categoria_id'=>2,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'OBSERVATORIO',
            'categoria_id'=>2,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'PLANETARIO',
            'categoria_id'=>2,
        ]);

        //3
        CentrosCategoria::query()->create([
            'nombre'=>'ESPACIO MAKER',
            'categoria_id'=>3,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'LABORATORIO DE INNOVACIÓN',
            'categoria_id'=>3,
        ]);

        //4
        CentrosCategoria::query()->create([
            'nombre'=>'MUSEO DE HISTORIA NATURAL',
            'categoria_id'=>4,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'MUSEO DE HISTORIA NATURAL (GEOLÓGICO)',
            'categoria_id'=>4,
        ]);

        CentrosCategoria::query()->create([
            'nombre'=>'CENTRO INTERACTIVO',
            'categoria_id'=>4,
        ]);



    }
}
