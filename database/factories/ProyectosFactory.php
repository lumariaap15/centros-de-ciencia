<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(\App\Proyecto::class, function (Faker $faker) {
    return [
        'nombre'=>$faker->sentence,
        'categoria_id'=>$faker->numberBetween(5,20),
        'ASCTEL'=>$faker->boolean,
        'centro_id'=>$faker->numberBetween(1,160)
    ];
});
