<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->unsignedBigInteger('categoria_id');
            $table->string('departamento_codigo')->index();
            $table->string('municipio_codigo');
            $table->string('owner')->nullable(); //A QUIEN PERTENECE EL CC
            $table->text('descripcion')->nullable();
            $table->string('direccion');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('pagina_web')->nullable();
            $table->string('correo')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();
            $table->date('fecha_creacion')->nullable();
            $table->string('audio')->nullable();

            //LLAVES FORANEAS
            $table->foreign('categoria_id')->references('id')->on('centros_categorias')
                ->onDelete('cascade');
            $table->foreign('departamento_codigo')->references('codigo')->on('departamentos')
                ->onDelete('cascade');
            $table->foreign('municipio_codigo')->references('codigo')->on('municipios')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros');
    }
}
