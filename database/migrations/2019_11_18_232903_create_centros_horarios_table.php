<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros_horarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            /*
            $table->enum('dia_semana',
                ['Lunes', 'Martes', 'Miércoles', 'Jueves','Viernes','Sábado','Domingo','Festivos']);
            */
            $table->string('horario');
            $table->unsignedBigInteger('centro_id');
            $table->timestamps();
            $table->foreign('centro_id')->references('id')->on('centros')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros_horarios');
    }
}
