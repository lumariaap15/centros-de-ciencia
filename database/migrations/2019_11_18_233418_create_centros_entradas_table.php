<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros_entradas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('valor');
            $table->text('nombre');
            $table->unsignedBigInteger('centro_id');
            $table->timestamps();
            $table->foreign('centro_id')->references('id')->on('centros')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros_entradas');
    }
}
