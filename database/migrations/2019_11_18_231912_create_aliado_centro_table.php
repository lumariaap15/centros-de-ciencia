<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAliadoCentroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aliado_centro', function (Blueprint $table) {
            $table->unsignedBigInteger('centro_id');
            $table->unsignedBigInteger('aliado_id');

            $table->foreign('centro_id')->references('id')->on('centros')
                ->onDelete('cascade');
            $table->foreign('aliado_id')->references('id')->on('aliados')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aliado_centro');
    }
}
