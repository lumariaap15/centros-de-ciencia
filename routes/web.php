<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Centro;

Route::group(['middleware' => ['auth']], function () {
    Route::resource('admin/centros','CentrosController');
    Route::resource('admin/centros/entradas','EntradasController')->except(['index', 'show']);
    Route::resource('admin/centros/proyectos','ProyectosController')->except(['index', 'show','create']);
    Route::get('admin/centros/entradas/{centroId}','EntradasController@index')->name('entradas.index');
    Route::resource('admin/centros/servicios','ServiciosController')->except(['index', 'show']);
    Route::resource('admin/aliados','AliadosController')->except(['show']);
    Route::get('admin/centros/servicios/{centroId}','ServiciosController@index')->name('servicios.index');
    Route::get('admin/centros/proyectos/{centroId}','ProyectosController@index')->name('proyectos.index');
    Route::get('admin/centros/proyectos/create/{centroId}','ProyectosController@create')->name('proyectos.create');
    Route::get('admin/centros/aliados/{centroId}','CentrosController@indexAliados')->name('centros.aliados');
    Route::post('admin/centros/aliados/asociar/{centroId}','CentrosController@asociarAliados')->name('centros.asociarAliados');
    Route::delete('admin/centros/aliados/delete/{centroId}/{id}','CentrosController@borrarAliados')->name('centros.borrarAliados');
    Route::resource('admin/centros/imagenes','ImagenesController')->except(['index', 'show']);
    Route::get('admin/centros/imagenes/{centroId}','ImagenesController@index')->name('imagenes.index');
    Route::resource('admin/centros/horarios','HorariosController')->except(['index', 'show']);
    Route::get('admin/centros/horarios/{centroId}','HorariosController@index')->name('horarios.index');
    Route::delete('admin/centros/audio/{centroId}','CentrosController@destroyAudio')->name('centros.destroyAudio');
});


//Auth::routes();

Route::get('login','Auth\LoginController@showLoginForm')->name('login')->middleware(['guest']);
Route::post('login','Auth\LoginController@login')->middleware(['guest']);
Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::get('centros/subtipos/{tipoId}','CentrosController@getSubtipos');
Route::get('centros/departamentos/{departamentoId}','CentrosController@getMunicipios');
Route::get('get-aliados/{centroId}','CentrosController@getAliados');

/** RUTAS FRONT */
Route::get('/','FrontController@inicio')->name('front.inicio');
Route::get('/centros','FrontController@centros')->name('front.centros');
Route::get('/mapa','FrontController@mapa')->name('front.mapa');
Route::get('/proyectos','FrontController@proyectos')->name('front.proyectos');
Route::post('/proyectos-filter','FrontController@proyectosPartial')->name('front.proyectos.partial');
Route::get('/aliados','FrontController@aliados')->name('front.aliados');
Route::post('/aliados-filter','FrontController@aliadosPartial')->name('front.aliados.partial');
Route::get('/centros/detalles/{centroId}','FrontController@detallesCentro')->name('front.centros.detalles');
Route::get('/centros/detalles/proyecto/{proyectoId}','FrontController@detallesProyecto')->name('front.centros.detalles.proyecto');
Route::get('/filter-centros', 'FrontController@inputCentros');
Route::get('/filter-centros-show/{id}', function ($id){
    $centro = Centro::query()->with('subtipo')->findOrFail($id);
    return response()->json($centro);
});

