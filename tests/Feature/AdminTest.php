<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * @test
     */
    public function unUsuarioNoAutenticadoNoPuedeIngresarAlAdmin()
    {
        $response = $this->get('admin/centros');

        return $response->assertRedirect('/login');
    }

}
