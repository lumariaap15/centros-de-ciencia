<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FrontTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function unUsuarioNoAutenticadoPuedeListarCentros()
    {
        $response = $this->get('/centros');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function unUsuarioNoAutenticadoPuedeVerMapa()
    {
        $response = $this->get('/mapa');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function unUsuarioNoAutenticadoPuedeVerProyectos()
    {
        $response = $this->get('/proyectos');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function unUsuarioNoAutenticadoPuedeVerAliados()
    {
        $response = $this->get('/mapa');

        $response->assertStatus(200);
    }
}
