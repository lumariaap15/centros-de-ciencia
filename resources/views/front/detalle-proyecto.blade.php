<div class="card-detalle">
    <div class="card-blanca {{($proyecto->subtipo->categoria_id === \App\CentrosCategoria::BIOESPACIOS_ID) ? 'my-border-card--verde' :
                (($proyecto->subtipo->categoria_id === \App\CentrosCategoria::CIENCIAS_ID) ? 'my-border-card--rojo' : (
                ($proyecto->subtipo->categoria_id === \App\CentrosCategoria::CIUDADANOS_ID) ? 'my-border-card--amarillo': 'my-border-card--azul'))}}">
        <h5 class="text-right card-detalle__icon" onclick="closeModal()">&times;</h5>
        <h1 class="card-blanca__titulo">{{$proyecto->nombre}}</h1>
        <h2 class="mb-3">{{$proyecto->centro->nombre}}</h2>
        {{--
        <h3 class="mb-3">{{$proyecto->centro->departamento->descripcion.', '.$proyecto->centro->municipio->descripcion}}</h3>
        --}}
        <p>{{$proyecto->ASCTEL ? 'Apropiación Social de Ciencia, Tecnología e Innovación (ASCTeI)' : 'Proyectos y actividades distintas a ASCTeI'}}</p>
        <p class="text-justify">{{$proyecto->descripcion}}</p>
    </div>
</div>
