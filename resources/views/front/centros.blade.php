@extends('front.layout')
@section('contenido')
    <div class="pt-3">
        <h1 class="my-5 card-blanca__titulo" >
            COMPRENSIONES DESDE LA POLÍTICA PÚBLICA
        </h1>
        <p>
            Los Centros de Ciencia se conciben como espacios idóneos para el intercambio, la comprensión y el uso contextualizado y democrático de la Ciencia, la Tecnología y la Innovación (CTI) por parte de la sociedad. Su propósito es construir lenguajes comunes para entablar diálogos abiertos, plurales y diversos; asimismo, potenciar el acceso a la información y al intercambio de conocimientos de un modo inspirador y entretenido, conservando siempre el rigor científico. El público es considerado <i>cocreador, cogestor y corresponsable</i> de los contenidos y de las experiencias de cada uno de estos espacios, de manera que, tanto individuos como comunidades, puedan servirse de las reflexiones sobre CTI para la transformación de sus propias realidades.</p>

            <p><strong>¿Qué dice la normatividad?</strong></p>



            <p>Las Resoluciones 1473 de 2016 y 0143 de 2017, emitidas por Colciencias, adoptan la Política de Actores del Sistema Nacional de Ciencia, Tecnología e Innovación en Colombia y regulan lo relativo al reconocimiento de los Centros de Ciencia. El Documento n.º 1602, adoptado mediante la Resolución 1473 de 2016, los define así:</p>



        <p class="pl-5">Instituciones de carácter público, privado o mixto, sin ánimo de lucro, con personería jurídica o dependientes de otra organización, con una planta física abierta al público de manera permanente y que tienen la Apropiación Social de la CTI (ASCTI) como parte integral de su misión u objeto social. Asimismo, reconocen la diversidad cultural, económica y social de las comunidades, promueven los principios de acceso democrático a la información y al conocimiento, y contribuyen a fortalecer la cultura CTeI en el país mediante programas y actividades educativas.</p>



        <p><strong>¿Qué es la Apropiación Social de Ciencia, Tecnología e Innovación (ASCTeI)?</strong></p>



            <p>De acuerdo con la política pública, se trata de:</p>

        <p class="pl-5">[…] un proceso intencionado de comprensión e intervención en las relaciones entre ciencia, tecnología y sociedad, que tiene como objetivo ampliar las dinámicas de generación, circulación y uso del conocimiento científico-tecnológico, y propiciar las sinergias entre sectores académicos, productivos, estatales, incluyendo activamente a las comunidades y grupos de interés de la sociedad civil. (Departamento Nacional de Planeación-Colciencias, 2015)</p>



        <p>Conozca a continuación cuáles son estos espacios y los cuatro grupos en que se clasifican.</p>

        <div class="accordion" id="accordionExample">
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#bioespacios">
                    <div class="mycard__header" id="headingOne">
                        <p class="mycard__sm-title mb-0">GRUPO 01</p>
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0 text-cc-verde">BIOESPACIOS</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="bioespacios" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <div class="row">
                            <div class="col col-12 col-lg-6 p-0 pr-lg-4">
                                <ul class="text-cc-verde">
                                    <li class="mb-3">
                                        Tienen colecciones biológicas o réplicas de colecciones biológicas.
                                    </li>
                                    <li class="mb-3">
                                        Facilitan la interpretación, la comunicación, las narrativas, la exhibición, conservación e investigación y/o la transmisión de conocimientos científicos y tecnológicos relacionados con las ciencias de la vida y de la salud, la evolución humana, la medicina y todas aquellas disciplinas que se ocupan de fenómenos biológicos.
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-12 col-lg-6 p-0 pl-lg-2">
                                <p>
                                    En la actualidad, los bioespacios están pasando de tener colecciones de seres vivos con fines educativos y exotistas, a convertirse en escenarios que promueven el valor de la vida y la conservación de la biodiversidad del país. Esta idea no está fundada únicamente sobre la contemplación, sino justo lo contrario: implica el uso y la apropiación. Por ejemplo, sucede con los bioespacios lo mismo que con las frutas y productos agrícolas autóctonos, en la medida en que se usan (aprovechan); también se promueve su cultivo, su apropiación y su cuidado.
                                </p>
                                <p>
                                    Por otro lado, en un bioespacio, la interactividad no se centra en el uso de la tecnología, sino en la creación de ambientes y oportunidades para que las personas exploren espontáneamente el lugar, enfocándose en la experiencia sensorial y colectiva con la naturaleza. Así, la labor de mediación de estos centros no solo permite el acercamiento de las comunidades, también los consolida como infraestructuras culturales que la sociedad apropia en la medida que las emplea en pro de sus intereses.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#ciencias">
                    <div class="mycard__header" id="headingOne">
                        <p class="mycard__sm-title mb-0">GRUPO 02</p>
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0 text-cc-rojo">ESPACIOS PARA LAS CIENCIAS EXACTAS, FÍSICAS, SOCIALES Y LA TECNOLOGÍA</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="ciencias" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <div class="row">
                            <div class="col col-12 col-lg-6 p-0 pr-lg-4">
                                <ul class="text-cc-rojo">
                                    <li class="mb-3">
                                        Tienen colecciones de objetos naturales o artificiales.
                                    </li>
                                    <li class="mb-3">
                                        Facilitan la interpretación, la comunicación, las narrativas, la exhibición, conservación e investigación y/o la transmisión de conocimientos científicos y tecnológicos relacionados con las ciencias exactas, físicas y químicas, la astronomía, la geología, la arqueología, la antropología, las ciencias de la computación y la informática y en general de todas aquellas disciplinas que se ocupan de fenómenos matemáticos, físicos, sociales y tecnológicos.
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-12 col-lg-6 p-0 pl-lg-2">
                                <p>
                                    Para comprender la naturaleza de los Centros de Ciencia del grupo 2, piense en un mapa del tamaño de un país entero. Imagine que cada detalle dibujado en el papel coincide en escala con lo que existe en la realidad. Ese sería el resultado tras la extraordinaria tarea cartográfica de disponer un catálogo de las mismas dimensiones del territorio, para clasificarlo y comprenderlo —la idea corresponde a «Del rigor de la ciencia» (1946), minicuento del escritor argentino Jorge Luis Borges. A esto se asemeja la labor emprendida por este grupo, el cual está conformado por museos de ciencias naturales, con sus herbarios y conjuntos de animales; museos de arqueología, con su acervo de piezas patrimoniales y bienes culturales; museos de geología, con sus colecciones paleontológicas y mineralógicas; entre otros. Es, en síntesis, la radiografía masiva de las características naturales y culturales de Colombia.
                                </p>
                                <p>
                                    Lo anterior supone que uno de los méritos de estos espacios, con su diverso arsenal de valores materiales e inmateriales, es permitirle a individuos y colectivos reconocerse como miembros de una comunidad. De esta manera, la memoria, la identidad, el patrimonio y las fuentes de investigación —elementos propios de estos escenarios— posibilitan la preservación del territorio y la cultura, y su proyección hacia el futuro; en otras palabras, son la constancia de lo que hemos sido como territorio y, por ende, constituyen una base sólida para comprender lo que somos y tenemos.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#ciudadanos">
                    <div class="mycard__header" id="headingOne">
                        <p class="mycard__sm-title mb-0">GRUPO 03</p>
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0 text-cc-amarillo">ESPACIOS DE CONSTRUCCIÓN CIUDADANA EN CTEL</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="ciudadanos" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <div class="row">
                            <div class="col col-12 col-lg-6 p-0 pr-lg-4">
                                <ul class="text-cc-amarillo">
                                    <li class="mb-3">
                                        Poseen un conjunto de bienes, instrumentos y herramientas cuya disposición carece de significado museológico, es decir, no conforma una colección en sentido estricto.
                                    </li>
                                    <li class="mb-3">
                                        Se valen, en ocasiones, de propuestas escenográficas o montajes interactivos para llevar a cabo sus procesos de ASCTeI.
                                    </li>
                                    <li class="mb-3">
                                        Facilitan al público sus bienes, instrumentos y herramientas, a fin de que este modele soluciones innovadoras para las problemáticas que pueda enfrentar una comunidad; esto, a partir de la comprensión e investigación de principios científicos y tecnológicos provenientes de las ciencias físicas y de la vida, la computación, la ingeniería, las artes digitales, y todas aquellas otras disciplinas en el ámbito de las nuevas tecnologías.
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-12 col-lg-6 p-0 pl-lg-2">
                                <p>La relación del grupo 3 con la ASCTeI tiene dos enfoques principales. De un lado, se encuentran los centros que enfatizan sus acciones en la dimensión educativa, léase espacios <i>maker</i>, FabLab y los espacios de cacharreo (<i>tinkering</i>). Estos se entienden como escenarios de creación que facilitan un aprendizaje activo, toda vez que involucran y animan a sus participantes a desarrollar, diseñar y crear sus propios contenidos, proyectos y productos. De otro lado, figuran los centros que se distinguen por su impacto colectivo, y por la colaboración entre personas que trabajan a fin de materializar proyectos para un bien común (espacios <i>hacker</i>, Minga Labs, colaboratorios, laboratorios de innovación, cafés de reparación, y otros).</p>
                                <p>Estos espacios aún son escasos y poco visibles en comparación con el universo de los otros tipos de Centros de Ciencia en Colombia. Por lo general, los escenarios del grupo 3 se convierten en experimentos sociotécnicos, en la medida que plantean una lógica de <i>hágalo usted mismo y con otros</i>. De allí que, a manera de agencia ciudadana, en ellos se propongan lógicas más allá del reciclaje, por ejemplo, la reutilización y la reparación, estrategias que contravienen la producción en masa porque privilegian la economía circular.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#mixtos">
                    <div class="mycard__header" id="headingOne">
                        <p class="mycard__sm-title mb-0">GRUPO 04</p>
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0 text-cc-azul">ESPACIOS MIXTOS</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="mixtos" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <div class="row">
                            <div class="col col-12 col-lg-6 p-0 pr-lg-4">
                                <ul class="text-cc-azul">
                                    <li class="mb-3">
                                        Combinan colecciones biológicas y colecciones de objetos con conjuntos de bienes, instrumentos y herramientas que carecen de significado museológicos.
                                    </li>
                                    <li class="mb-3">
                                        Facilitan la interpretación, la comunicación, las narrativas, la exhibición, conservación e investigación y/o la transmisión de conocimientos científicos y tecnológicos provenientes de diferentes disciplinas científicas, tanto biológicas como no biológicas.
                                    </li>
                                    <li class="mb-3">
                                        Facilitan al público sus bienes, instrumentos y herramientas para que este diseñe y modele soluciones innovadoras a las diferentes problemáticas que pueda enfrentar una comunidad.
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-12 col-lg-6 p-0 pl-lg-2">
                                <p>Este último grupo, constituido esencialmente por centros interactivos y parques temáticos, está dedicado a generar experiencias en sus públicos. La propuesta de mediación que fundamenta a estos centros de ciencia plantea ofrecer a los visitantes una vivencia cercana a los fenómenos naturales, físicos y tecnológicos; esto con el interés de brindarles la confianza y las habilidades para comprender el mundo que los rodea. De esta forma, se diseñan aparatos con el objetivo de presentar y permitir la interacción con estos fenómenos, a lo que se le llama «exposiciones interactivas». A través de estos artefactos, se espera que el público sea capaz de conducir la actividad, reunir evidencias, seleccionar entre alternativas, sacar conclusiones, entre otros presupuestos.</p>
                                <p>En resumen, el propósito de estos escenarios es comunicar y formar a los diferentes públicos sobre ciencias y tecnologías, abrazando la premisa de que el aprendizaje de los conocimientos en ciencia, tecnología e innovación es de carácter decisivo para toda ciudadanía y para la sociedad colombiana en especial.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
