@extends('front.layout')
@section('styles')
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 600px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
            min-height: 80vh;
        }
    </style>
@endsection
@section('contenido')
    <p id="map-option-selected-container" style="margin-top: 10px; display: none">Ubicación de: <span id="map-option-selected">TODOS</span>
        <a href="javascript:void(0)" onclick="volver()">Volver</a></p>
    <div id="map"></div>
@endsection
@section('scripts')
    <script>
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 second for example
        var $input = $('#input-centros');
        //user is "finished typing," do something
        function doneTyping () {
            loading()
            setTimeout(function () {
                updateMarkers()
            }, 500)
        }

        $(function () {
            filtrarMapa = true;
            $input.keyup(()=>{
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            });
            $input.keydown(function () {
                clearTimeout(typingTimer);
            });

            $(".mysidebar__tipo").click(()=>{
                loading()
                setTimeout(function () {
                    updateMarkers()
                }, 500)
            })
        });

        var allMarkers = [];
        var map;
        // Initialize and add the map
        function initMap() {
            map = new google.maps.Map(
                document.getElementById('map'), {zoom: 6, center: {lat: 4.0857809, lng: -78.2249637}});
            $.get('/filter-centros',function (centros) {
                centrosArr = centros;
                initMarkers()
                /*for(var i = 0; i<centros.length; i++){
                    var location = {lat: parseFloat(centros[i].latitud.replace(',','.')), lng: parseFloat(centros[i].longitud.replace(',','.'))};
                    var icono = "";
                    if(Number(centros[i].subtipo.categoria_id) === 1){
                        icono = "/images/mapa/Elementos-graficos-Bioespacio.svg"
                    }else if(Number(centros[i].subtipo.categoria_id) === 2){
                        icono = "/images/mapa/Elementos-graficos-cienciaexactas.svg"
                    }else if(Number(centros[i].subtipo.categoria_id) === 3){
                        icono = "/images/mapa/Elementos-graficos-espacioconstruccion.svg"
                    }else{
                        icono = "/images/mapa/Elementos-graficos-espaciosmixtos.svg"
                    }
                    var icon2 = {
                        url: icono, // url
                        scaledSize: new google.maps.Size(50, 50), // scaled size
                        origin: new google.maps.Point(0,0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    };
                    var marker = new google.maps.Marker(
                        {
                            position: location,
                            map: map,
                            animation:google.maps.Animation.DROP,
                            icon: icon2
                        }
                    );
                    allMarkers.push(marker);
                    google.maps.event.addListener(marker, 'click', (function(centros,i) {
                        return function() {
                            mostrarModalDetalle(centros[i].id)
                        }
                    })(centros,i));
                }*/
            });
            /*
            // The location of Uluru
            var uluru = {lat: -25.344, lng: 131.036};
            // The map, centered at Uluru

            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});

             */
        }

        function volver() {
            loading()
            updateMarkers()
            $("#map-option-selected-container").hide()
        }

        function updateMarkers(id = null) {
            if(optionSelected !== 'los filtros seleccionados'){
                $("#map-option-selected-container").fadeIn()
                $("#map-option-selected").text(optionSelected)
            }else{
                $("#map-option-selected-container").hide()
                $("#map-option-selected").text('')
            }

            allMarkers.forEach(item=>{
                item.setMap(null)
            });
            initMarkers(id)
            loadingClose()
        }

        function initMarkers(id = null) {
            if(id !== null){
                $.get('/filter-centros-show/'+id, function (resp) {
                    initMarkers2([resp])
                })
            }else{
                initMarkers2(centrosArr)
            }
        }

        function initMarkers2(centros) {
            for(var i = 0; i<centros.length; i++){
                var location = {lat: parseFloat(centros[i].latitud.replace(',','.')), lng: parseFloat(centros[i].longitud.replace(',','.'))};
                var icono = "";
                if(Number(centros[i].subtipo.categoria_id) === 1){
                    icono = "/images/mapa/Elementos-graficos-Bioespacio.svg"
                }else if(Number(centros[i].subtipo.categoria_id) === 2){
                    icono = "/images/mapa/Elementos-graficos-cienciaexactas.svg"
                }else if(Number(centros[i].subtipo.categoria_id) === 3){
                    icono = "/images/mapa/Elementos-graficos-espacioconstruccion.svg"
                }else{
                    icono = "/images/mapa/Elementos-graficos-espaciosmixtos.svg"
                }
                var icon2 = {
                    url: icono, // url
                    scaledSize: new google.maps.Size(50, 50), // scaled size
                    origin: new google.maps.Point(0,0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };
                var marker = new google.maps.Marker(
                    {
                        position: location,
                        map: map,
                        //animation:google.maps.Animation.DROP,
                        icon: icon2
                    }
                );
                allMarkers.push(marker);
                google.maps.event.addListener(marker, 'click', (function(centros,i) {
                    return function() {
                        mostrarModalDetalle(centros[i].id, centros[i].nombre, false, true)
                    }
                })(centros,i));
            }
        }
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVLquOIOewzy6Ok2AyYbrMCgJ3OX1Lay4&callback=initMap">
    </script>
@endsection
