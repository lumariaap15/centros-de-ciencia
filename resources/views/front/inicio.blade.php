<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Centros de ciencia</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/aos-master/dist/aos.css')}}" />
    <style>
        body{
            background: black;
            color: white;
        }
        .mynavbar__link a{
            color: white;
        }
        .mynavbar__link::after{
            background: white;
        }

        @-webkit-keyframes fadeInUp {
            from {
                opacity:0;
                -webkit-transform:translate3d(0, 20%, 0);
                transform:translate3d(0, 20%, 0);
            }
            to {
                opacity:1;
                -webkit-transform:none;
                transform:none;
            }
        }
        @keyframes fadeInUp {
            from {
                opacity:0;
                -webkit-transform:translate3d(0, 20%, 0);
                transform:translate3d(0, 20%, 0);
            }
            to {
                opacity:1;
                -webkit-transform:none;
                transform:none;
            }
        }

        @-webkit-keyframes fadeInLeft {
            from {
                opacity:0;
                -webkit-transform:translate3d(-20%, 0, 0);
                transform:translate3d(-20%, 0, 0);
            }
            to {
                opacity:1;
                -webkit-transform:none;
                transform:none;
            }
        }
        @keyframes fadeInLeft {
            from {
                opacity:0;
                -webkit-transform:translate3d(-20%, 0, 0);
                transform:translate3d(-20%, 0, 0);
            }
            to {
                opacity:1;
                -webkit-transform:none;
                transform:none;
            }
        }
        @-webkit-keyframes fadeInRight {
            from {
                opacity:0;
                -webkit-transform:translate3d(20%, 0, 0);
                transform:translate3d(20%, 0, 0);
            }
            to {
                opacity:1;
                -webkit-transform:none;
                transform:none;
            }
        }
        @keyframes fadeInRight {
            from {
                opacity:0;
                -webkit-transform:translate3d(20%, 0, 0);
                transform:translate3d(20%, 0, 0);
            }
            to {
                opacity:1;
                -webkit-transform:none;
                transform:none;
            }
        }
        .link-scroll-up{
            position: fixed;
            bottom: 0;
            left: 50%;
            transform: translateX(-50%);
        }
        .link-scroll-up .triangle{
            opacity: 0;
            position: relative;
            top: 50%;
            transition: all .3s;
        }
        .link-scroll-up:hover .triangle{
            opacity: 1;
            top: 0;
        }
        .custom-nav{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            background: black;
            z-index: 1000;
        }
        [data-aos=fade-up]{
            transform:translate3d(0,150px,0);
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark" style="position: fixed; top: 0; left: 0; right: 0; background: black; z-index: 10">
    <a class="navbar-brand" href="#"><img src="{{asset('images/LogoCPF.png')}}" width="250px" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <div class="mynavbar__link"><a href="{{route('front.centros')}}">CENTROS DE CIENCIA</a></div>
            </li>
            <li class="nav-item">
                <div class="mynavbar__link"><a href="{{route('front.mapa')}}">DÓNDE ESTAMOS</a></div>
            </li>
            <li class="nav-item">
                <div class="mynavbar__link"><a href="{{route('front.proyectos')}}">PROYECTOS</a></div>
            </li>
            <li class="nav-item">
                <div class="mynavbar__link"><a href="{{route('front.aliados')}}">ALIADOS</a></div>
            </li>
        </ul>
    </div>
</nav>
{{--
<div class="custom-nav">
    <div class="container">
        <div class="row px-3 pt-2">
            <div class="col col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                <div class="p-0">
                    <div class="d-flex justify-content-between">
                        <img src="{{asset('images/LogoCPF.png')}}" width="100%" alt="">
                    </div>
                </div>
            </div>
            <div class="col col-12 col-sm-6 col-md-8 col-lg-9 mb-3">
                <div style="height: 100%" class="d-flex justify-content-end align-items-md-center flex-column text-right flex-md-row">
                    <div class="mynavbar__link {{$activeMenu === 1 ? 'active' : ''}}"><a href="{{route('front.centros')}}">CENTROS DE CIENCIA</a></div>
                    <div class="mynavbar__link {{$activeMenu === 2 ? 'active' : ''}}"><a href="{{route('front.mapa')}}">DÓNDE ESTAMOS</a></div>
                    <div class="mynavbar__link {{$activeMenu === 3 ? 'active' : ''}}"><a href="{{route('front.proyectos')}}">PROYECTOS</a></div>
                    <div class="mynavbar__link {{$activeMenu === 4 ? 'active' : ''}}"><a href="{{route('front.aliados')}}">ALIADOS</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
--}}

<div class="container" style="overflow: hidden">

    <div class="row">
        <div class="col col-12 col-md-6 col-lg-7 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-02.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="150" data-aos-duration="1200">
            <img src="{{asset('images/front/lorito.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-4 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1200">
            <img src="{{asset('images/front/Sala música 2.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-8 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="180" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-03.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-3 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1200">
            <img src="{{asset('images/front/orquidead.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-4 px-1 mb-5 py-5" data-aos="fade-up" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-04.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="150" data-aos-duration="1200">
            <img src="{{asset('images/front/pajaro-amarillo.png')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1200">
            <img src="{{asset('images/front/Museo José Royo y Gómez Sede Cali 1 – Web.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-4 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="20" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-05.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-3 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="250" data-aos-duration="1200">
            <img src="{{asset('images/front/Planetario de Combarranquilla 5 - Web.png')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-6 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="10" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-06.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-6 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="120" data-aos-duration="1200">
            <img src="{{asset('images/front/Cisc + Laboratorio De Ideas 7 - FB.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="290" data-aos-duration="1200">
            <img src="{{asset('images/front/Acuario 1.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-4 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="20" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-07.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-3 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1200">
            <img src="{{asset('images/front/Maloka 9 - FB.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1200">
            <img src="{{asset('images/front/Fundación Maikuchiga 3 - FB.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-7 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="310" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-08.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-6 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="80" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-09.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-6 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="150" data-aos-duration="1200">
            <img src="{{asset('images/front/Museo de Historia Natural Universidad Nacional de Colombia 3 - Web.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="20" data-aos-duration="1200">
            <img src="{{asset('images/front/Alto de los Ídolos y Alto de las Piedras 5 - Web.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-7 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="180" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-10.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-8 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-11.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-4 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="250" data-aos-duration="1200">
            <img style="transform: rotate(90deg)" src="{{asset('images/front/Museo de La Salle 3 - Correo.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-8 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="30" data-aos-duration="1200">
            <img src="{{asset('images/front/Laboratorio de Innovación, Creatividad y Nuevas Tecnologías Vive Lab Bogotá 5 - Correo.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 col-lg-4 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="160" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-12.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="45" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-13.svg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-6 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="90" data-aos-duration="1200">
            <img src="{{asset('images/front/Fundación Maikuchiga 4 - FB.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-5 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1200">
            <img src="{{asset('images/front/Centro de Ciencia Francisco José de Caldas 3 - Web.jpg')}}" width="100%" alt="">
        </div>
        <div class="col col-12 col-md-7 px-1 mb-5 py-5" data-aos="fade-up" data-aos-delay="30" data-aos-duration="1200">
            <img src="{{asset('images/front/inicio-2-14.svg')}}" width="100%" alt="">
        </div>
    </div>
    <div class="link-scroll-up row justify-content-center text-center">
        <a class="text-white d-inline-block" href="javascript:void(0)" onclick="scrollTopMethod()">
            <div class="triangle">&#9650;</div>
            <div class="triangle">&#9650;</div>
            <div class="triangle">&#9650;</div>
            <div style="background: black; padding: 10px 15px 0 15px">VOLVER ARRIBA</div>
        </a>
    </div>
</div>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('plugins/aos-master/dist/aos.js')}}"></script>
<script>
    AOS.init();
    var body = $("body");
    function scrollTopMethod() {
        console.log(body.scrollTop)
        $("html, body").animate({ scrollTop: "0" },3000);
    }
</script>
</body>
</html>
