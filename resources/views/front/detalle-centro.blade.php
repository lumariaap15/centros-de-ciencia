<div style="z-index: 5" class="card-detalle
{{
(intval($centro->subtipo->categoria_id) === \App\CentrosCategoria::BIOESPACIOS_ID) ?
 'card-detalle--verde' : (
                (intval($centro->subtipo->categoria_id) === \App\CentrosCategoria::CIENCIAS_ID) ?
                 'card-detalle--rojo' : (
                    (intval($centro->subtipo->categoria_id) === \App\CentrosCategoria::CIUDADANOS_ID) ?
                    'card-detalle--amarillo': 'card-detalle--azul'))}}">
    <h5 class="text-right text-white card-detalle__icon" onclick="closeModal()">&times;</h5>
    <h1 class="card-detalle__titulo">{{$centro->nombre}}</h1>
    <h2 class="text-white mb-3">{{(intval($centro->departamento->codigo) !== 11 ? $centro->departamento->descripcion.', ':'').$centro->municipio->descripcion}}</h2>
    <div class="card-detalle__info mb-3">
        @if($centro->fecha_creacion !== null && $centro->fecha_creacion !== '')
            <p class="mb-0 text-white">Fecha de creación: {{$centro->fecha_creacion}}</p>
        @endif
        <p class="mb-0 text-white">Tipo: {{$centro->subtipo->padre->nombre}}</p>
        <p class="mb-0 text-white">Subtipo: {{$centro->subtipo->nombre}}</p>
        @if($centro->owner !== null && $centro->owner !== '')
            <p class="mb-0 text-white">Pertenece a: {{$centro->owner}}</p>
        @endif
    </div>
    @if($centro->descripcion !== null && $centro->descripcion !== '')
        <p class="text-white text-justify">{{$centro->descripcion}}</p>
    @endif
    @if(count($centro->imagenes)>0)
        <div class="row justify-content-center">
            <div class="col col-12 col-md-10 col-lg-8">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($centro->imagenes as $key=>$img)
                            <li data-target="#carouselExampleSlidesOnly" data-slide-to="{{$key}}" class="{{$key===0?'active':''}}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($centro->imagenes as $key=>$img)
                            <div class="carousel-item {{$key===0?'active':''}}">
                                <img class="d-block w-100" src="{{\Illuminate\Support\Facades\Storage::url($img->image)}}" alt="First slide">
                                <div class="carousel-caption d-none d-md-block mt-5">
                                    <p style="opacity: 0">{{$img->url}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleSlidesOnly" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleSlidesOnly" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="row justify-content-between my-3 text-white">
                    <p><strong>Foto: </strong><span id="slider-caption">{{$centro->imagenes[0]->url}}</span></p>
                    <p><&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="slider-actual-img">1</span> - {{count($centro->imagenes)}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;></p>
                </div>
            </div>
        </div>
    @endif
    <div class="accordion" id="accordionExample">
        @if(count($centro->proyectos)>0)
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#proyectos">
                    <div class="mycard__header" id="headingOne">
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0">PROYECTOS</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>


                <div id="proyectos" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <div class="row">
                            @foreach($centro->proyectos as $proyecto)
                                <div class="col col-12 col-md-6 col-lg-4 p-1">
                                    <a href="javascript:void(0)" onclick="mostrarModalDetalleProyecto('{{$proyecto->id}}')">
                                        <div class="p-3 my-border-card {{$proyecto->subtipo->categoria_id === \App\CentrosCategoria::BIOESPACIOS_ID ? 'my-border-card--verde' :
                $proyecto->subtipo->categoria_id === \App\CentrosCategoria::CIENCIAS_ID ? 'my-border-card--rojo' :
                $proyecto->subtipo->categoria_id === \App\CentrosCategoria::CIUDADANOS_ID ? 'my-border-card--amarillo': 'my-border-card--azul'}}">
                                            {{--
                                            <small class="text-white">{{(intval($proyecto->centro->departamento->codigo) !== 11 ? $proyecto->centro->departamento->descripcion.', ':'').$proyecto->centro->municipio->descripcion}}</small>
                                            --}}
                                            <p class="my-border-card__title text-white">{{$proyecto->nombre}}</p>
                                            <p class="text-justify text-white text-small">{{$proyecto->ASCTEL ? 'Apropiación Social de Ciencia, Tecnología e Innovación (ASCTeI)' : 'Proyectos y actividades distintas a ASCTeI'}}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        @endif
        @if(count($centro->horarios) > 0 || count($centro->entradas) > 0)
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#servicios">
                    <div class="mycard__header" id="headingOne">
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0">SERVICIOS</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="servicios" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <p class="text-white"><strong>HORARIO DE ATENCIÓN</strong></p>
                        @foreach($centro->horarios as $horario)
                            <p class="mb-0 text-white">{{$horario->horario}}</p>
                        @endforeach
                        <br>
                        <br>
                        <p class="text-white"><strong>VALOR ENTRADA</strong></p>
                        @foreach($centro->entradas as $entrada)
                            @if(intval($entrada->valor) !== 0)
                            <p class="mb-0 text-white">{{$entrada->nombre.' - $'.number_format($entrada->valor,0,',','.')}}</p>
                            @else
                                <p class="mb-0 text-white">{{$entrada->nombre}}</p>
                                @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if(count($centro->servicios)>0)
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#oferta">
                    <div class="mycard__header" id="headingOne">
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0">OFERTA GENERAL</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="oferta" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <ul>
                            @foreach($centro->servicios as $servicio)
                                <li class="text-white">{{$servicio->descripcion}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        @if(count($centro->aliados)>0)
            <div class="mycard">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#aliados">
                    <div class="mycard__header" id="headingOne">
                        <div class="d-flex justify-content-between">
                            <p class="mycard__lg-title mb-0">ALIADOS</p>
                            <p class="mycard__icon align-self-end">+</p>
                        </div>
                    </div>
                </a>

                <div id="aliados" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="mycard__body py-5">
                        <ul>
                            @foreach($centro->aliados as $aliado)
                                <li><a href="{{$aliado->url}}" target="_blank" class="text-white">{{$aliado->nombre}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="mycard">
            <a href="javascript:void(0)" data-toggle="collapse" data-target="#contacto">
                <div class="mycard__header" id="headingOne">
                    <div class="d-flex justify-content-between">
                        <p class="mycard__lg-title mb-0">CONTACTO</p>
                        <p class="mycard__icon align-self-end">+</p>
                    </div>
                </div>
            </a>
            <div id="contacto" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="mycard__body py-5">
                    <p class="mb-0 text-white">{{$centro->direccion}}</p>
                    <p class="mb-0 text-white">{{$centro->correo}}</p>
                    <p class="mb-0"><a class="text-white" href="{{$centro->pagina_web}}" target="_blank">{{$centro->pagina_web}}</a></p>
                    <p class="mb-0 text-white">{{$centro->celular !== null ? ($centro->telefono !== null ? ($centro->telefono.' - +57 '.$centro->celular) : '+57 '.$centro->celular) : $centro->telefono}}</p>
                </div>
            </div>

        </div>
    </div>
    @if($centro->audio !== null)
        <div class="row justify-content-center">
            <div class="mt-3" style="width: auto">
                <audio preload="auto">
                    <source src="{{\Illuminate\Support\Facades\Storage::url($centro->audio)}}">
                </audio>
            </div>
        </div>
    @endif
</div>
<script>
    audiojs.events.ready(function() {
        var as = audiojs.createAll();
    });
</script>
<script>
    $('#carouselExampleSlidesOnly').on('slide.bs.carousel', function () {
        setTimeout(function () {
            var activeCaption = $('#carouselExampleSlidesOnly .carousel-item.active p').text()
            $("#slider-caption").text(activeCaption)
            var activeIdx = $('#carouselExampleSlidesOnly .carousel-indicators .active').data('slide-to')
            $("#slider-actual-img").text(Number(activeIdx)+1)
        },1000)
    })
</script>
