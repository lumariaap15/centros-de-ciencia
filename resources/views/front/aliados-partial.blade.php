<div class="col-12 p-0">
    <p id="message-aliados-container" style="display: none">Aliados de: <span id="message-aliados"></span>. <a href="javascript:void(0)" onclick="volver()">Volver</a></p>
</div>
@if(count($aliados) > 0)
    <div class="col col-12 col-md-6 px-3">
        @for($i=0; $i<count($aliados); $i++)
            @if(strtoupper(substr($aliados[$i]->nombre,0,1)) <= "L")
                <p><a class="t-black mysidebar__link--azul" target="_blank" href="{{$aliados[$i]->url}}">{{$aliados[$i]->nombre}}</a></p>
            @endif
        @endfor
    </div>
    <div class="col col-12 col-md-6 px-3">
        @for($i=0; $i<count($aliados); $i++)
            @if(strtoupper(substr($aliados[$i]->nombre,0,1)) > "L")
                <p><a class="t-black mysidebar__link--azul" target="_blank" href="{{$aliados[$i]->url}}">{{$aliados[$i]->nombre}}</a></p>
            @endif
        @endfor
    </div>
@else
    <p>No se encontraron aliados para los filtros seleccionados.</p>

@endif
<script>
    $(function () {
        if(optionSelected !== 'los filtros seleccionados' && optionSelected !== ''){
            $("#message-aliados-container").fadeIn()
            $("#message-aliados").text(optionSelected)
        }else{
            $("#message-aliados-container").hide()
            $("#message-aliados").text('')
        }
    })
    function volver() {
        optionSelected = '';
        $("#message-aliados-container").hide()
        loading()
        updateAliados(centrosArr)
    }
</script>
