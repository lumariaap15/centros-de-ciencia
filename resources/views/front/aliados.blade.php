@extends('front.layout')
@section('styles')
   <style>
       .t-black{
            color: black;
       }
   </style>
@stop
@section('contenido')
    <div>
        <h1 class="my-5 card-blanca__titulo" >
            ESTOS SON LOS ALIADOS DE LOS CENTROS DE CIENCIA
        </h1>
        <div class="row" id="contenido">

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 second for example
        var $input = $('#input-centros');
        //user is "finished typing," do something
        function doneTyping () {
            loading()
            setTimeout(function () {
                updateAliados(centrosArr)
            }, 500)
        }

        $(function () {
            filtrarAliados = true;
            updateAliados('all')
            $input.keyup(()=>{
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            });
            $input.keydown(function () {
                clearTimeout(typingTimer);
            });

            $(".mysidebar__tipo").click(()=>{
                loading()
                setTimeout(function () {
                    updateAliados(centrosArr)
                }, 500)
            })
        });

        function updateAliados(centros) {
            let centrosOtro = [];
            if(centros !== 'all'){
                for(var i=0;i<centros.length;i++){
                    centrosOtro.push(centros[i].id)
                }
                centros = centrosOtro
            }
            $('#contenido').load('{{route('front.aliados.partial')}}', {
                centros: centros,
                _token: $("meta[name='csrf-token']").attr("content")
            }, function () {
                loadingClose();
            });
        }

    </script>
@endsection
