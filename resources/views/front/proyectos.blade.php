@extends('front.layout')
@section('contenido')
    <div class="mt-0 mt-sm-5 mt-md-0">
        <div class="row" id="contenido">

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 second for example
        var $input = $('#input-centros');
        //user is "finished typing," do something
        function doneTyping () {
            loading()
            setTimeout(function () {
                updateProyectos(centrosArr)
            }, 500)
        }

        $(function () {
            filtrarProyectos = true;
            updateProyectos('all')
            $input.keyup(()=>{
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            });
            $input.keydown(function () {
                clearTimeout(typingTimer);
            });

            $(".mysidebar__tipo").click(()=>{
                loading()
                setTimeout(function () {
                    updateProyectos(centrosArr)
                }, 500)
            })

        });

        function updateProyectos(centros) {
            let centrosOtro = [];
            if(centros !== 'all'){
                for(var i=0;i<centros.length;i++){
                    centrosOtro.push(centros[i].id)
                }
                centros = centrosOtro
            }
            $('#contenido').load('{{route('front.proyectos.partial')}}', {
                centros: centros,
                _token: $("meta[name='csrf-token']").attr("content")
            }, function () {
                loadingClose();
            });
        }

    </script>
    @endsection
