<div class="col-12 p-0">
<p id="message-aliados-container" style="display: none">Proyectos de: <span id="message-aliados"></span>. <a href="javascript:void(0)" onclick="volver()">Volver</a></p>
</div>
@if(count($proyectos)>0)
    @foreach($proyectos as $proyecto)
        <div class="col col-12 col-md-6 col-xl-4 px-0 px-md-2 py-3">
            <a href="javascript:void(0)" onclick="mostrarModalDetalleProyecto('{{$proyecto->id}}')">
                <div style="min-height: 300px" class="p-3 my-border-card {{($proyecto->subtipo->categoria_id === \App\CentrosCategoria::BIOESPACIOS_ID) ? 'my-border-card--verde' :
                (($proyecto->subtipo->categoria_id === \App\CentrosCategoria::CIENCIAS_ID) ? 'my-border-card--rojo' : (
                ($proyecto->subtipo->categoria_id === \App\CentrosCategoria::CIUDADANOS_ID) ? 'my-border-card--amarillo': 'my-border-card--azul'))}}">
                    {{--
                    <small class="text-black">{{$proyecto->centro->departamento->descripcion.', '.$proyecto->centro->municipio->descripcion}}</small>
                    --}}
                    <p class="my-border-card__title text-black">{{$proyecto->nombre}}</p>
                    <p class="text-black text-small">{{$proyecto->centro->nombre}}</p>
                    <p class="text-black text-small">{{$proyecto->ASCTEL ? 'Apropiación Social de Ciencia, Tecnología e Innovación (ASCTeI)' : 'Proyectos y actividades distintas a ASCTeI'}}</p>
                </div>
            </a>
        </div>
    @endforeach
@else
    <p>No se encontraron proyectos para los filtros seleccionados.</p>
@endif
<script>
    $(function () {
        if(optionSelected !== 'los filtros seleccionados' && optionSelected !== ''){
            $("#message-aliados-container").fadeIn()
            $("#message-aliados").text(optionSelected)
        }else{
            $("#message-aliados-container").hide()
            $("#message-aliados").text('')
        }
    })
    function volver() {
        optionSelected = '';
        $("#message-aliados-container").hide()
        loading()
        updateProyectos(centrosArr)
    }
</script>
