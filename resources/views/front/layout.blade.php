<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Centros de ciencia</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    @yield('styles')
    <style>
        .audiojs .play-pause {
            width: 40px;
        }
        #input-centros::placeholder{
            color: white;
        }
        #input-centros{
            font-size: 14px;
        }
        .scroll-hidden{
            overflow: hidden;
        }
        body{
            background: white;
        }
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-ripple div {
            position: absolute;
            border: 4px solid black;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
        }
        .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
        }
        @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
        }
        #loader-container{
            height: 100%;
            width: 100%;
            z-index: 100;
            background: rgba(255,255,255,.8);
            position: fixed;
            top: 0;
            right: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
        }

    </style>
</head>
<body>
<div id="loader-container">
    <div class="lds-ripple"><div></div><div></div></div>
</div>
<div class="row">
    <div class="col col-12 col-sm-6 col-lg-3 mysidebar p-4" id="sidebar">
        <div class="d-flex justify-content-between">
            <a href="{{route('front.inicio')}}"><div class="mb-3"><img src="{{asset('images/LogoCPF.png')}}" width="100%" alt=""></div></a>
            <div class="mynavbar__icon d-lg-none text-white" onclick="toggleSidebar()">&times;</div>
        </div>
        {{--
        <select onchange="changeTipo(this)" class="form-control mb-3 mysidebar__select">
            <option value="">Tipo</option>
            @foreach($tipos as $tipo)
                @if(strlen($tipo->nombre) < 20)
                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                @else
                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                @endif
            @endforeach
        </select>
        <select id="subtipos" onchange="changeSubtipo(this)" class="form-control mb-3 mysidebar__select">
            <option>Subtipo</option>
        </select>
        --}}
        <div class="mysidebar__tipo mysidebar__tipo--verde" id="tipo-bio">
            <div class="d-flex align-items-center" onclick="changeTipo(1,'tipo-bio')" >
                <div style="padding: 10px"><img src="{{asset('images/front/sidebar/Bio.svg')}}" alt="" width="30px"></div>
                <p class="mb-0">BIO</p>
            </div>
            <div class="mysidebar__subtipo"></div>
        </div>
        <div class="mysidebar__tipo mysidebar__tipo--rojo"  id="tipo-ciencias">
            <div class="d-flex align-items-center" onclick="changeTipo(2,'tipo-ciencias')">
                <div style="padding: 10px"><img src="{{asset('images/front/sidebar/ciencias_y_tecnologia.svg')}}" alt="" width="30px"></div>
                <p class="mb-0">CIENCIAS Y TECNOLOGÍA</p>
            </div>
            <div class="mysidebar__subtipo"></div>
        </div>
        <div class="mysidebar__tipo mysidebar__tipo--amarillo"  id="tipo-ciudadana">
            <div class="d-flex align-items-center" onclick="changeTipo(3,'tipo-ciudadana')">
                <div style="padding: 10px"><img src="{{asset('images/front/sidebar/Ciudadania.svg')}}" alt="" width="30px"></div>
                <p class="mb-0">CONSTRUCCIÓN CIUDADANA</p>
            </div>
            <div class="mysidebar__subtipo"></div>
        </div>
        <div class="mysidebar__tipo mysidebar__tipo--azul"  id="tipo-mixtos">
            <div class="d-flex align-items-center" onclick="changeTipo(4,'tipo-mixtos')">
                <div style="padding: 10px"><img src="{{asset('images/front/sidebar/Mixtos.svg')}}" alt="" width="30px"></div>
                <p class="mb-0">MIXTOS</p>
            </div>
            <div class="mysidebar__subtipo"></div>
        </div>
        <div class="d-flex align-items-center mb-3 pl-1 pt-4">
            <img src="{{asset('images/front/sidebar/Buscar.svg')}}" class="mr-2" height="25px" alt="">
            <input placeholder="Texto de búsqueda" id="input-centros" onkeyup="getCentros()" type="text" class="form-control myinput pl-0 py-2" style="height: 30px">
        </div>
        <p class="mb-0 text-right text-white" id="centros-result">160 Resultados</p>
        <div class="mysidebar__lista">
            <div id="list-centros-container">
                @foreach($centros as $centro)
                    <p style="line-height: 90%" class="mb-3"><a href="javascript:void(0)" onclick="mostrarModalDetalle('{{$centro->id}}','{{$centro->nombre}}',true)" class="mysidebar__link {{
(intval($centro->subtipo->categoria_id) === \App\CentrosCategoria::BIOESPACIOS_ID) ?
 'mysidebar__link--verde' : (
                (intval($centro->subtipo->categoria_id) === \App\CentrosCategoria::CIENCIAS_ID) ?
                 'mysidebar__link--rojo' : (
                    (intval($centro->subtipo->categoria_id) === \App\CentrosCategoria::CIUDADANOS_ID) ?
                    'mysidebar__link--amarillo': 'mysidebar__link--azul'))}}">{{$centro->nombre}}</a></p>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col col-12 col-lg-3">
        &nbsp;
    </div>
    <div class="col col-12 col-lg-9 p-2 p-md-5" id="general-content">
        <div class="p-0 p-md-5">
            <div class="container">
                <div class="d-flex justify-content-between mynavbar mb-5">
                    <div class="mynavbar__icon d-lg-none" onclick="toggleSidebar()">&#9776;</div>
                    <div class="d-none d-lg-block">&nbsp;</div>
                    <div class="d-flex flex-column text-right flex-sm-row">
                        <div class="mynavbar__link {{$activeMenu === 1 ? 'active' : ''}}"><a href="{{route('front.centros')}}">CENTROS DE CIENCIA</a></div>
                        <div class="mynavbar__link {{$activeMenu === 2 ? 'active' : ''}}"><a href="{{route('front.mapa')}}">DÓNDE ESTAMOS</a></div>
                        <div class="mynavbar__link {{$activeMenu === 3 ? 'active' : ''}}"><a href="{{route('front.proyectos')}}">PROYECTOS</a></div>
                        <div class="mynavbar__link {{$activeMenu === 4 ? 'active' : ''}}"><a href="{{route('front.aliados')}}">ALIADOS</a></div>
                    </div>
                </div>
                @yield('contenido')
            </div>
        </div>
    </div>
</div>
<div class="row" id="modal-container" style="display: none; z-index: 5">
    <div class="col col-12 col-lg-3 p-0">
        &nbsp;
    </div>
    <div id="modal-detalle-centro" class="col col-12 col-lg-9 p-0" style="position: fixed; top:0; right: 0; bottom: 0; z-index: 5; padding-top: 150px; overflow-y: scroll">

    </div>
</div>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('plugins/audiojs/audio.min.js')}}"></script>
<script>
    audiojs.events.ready(function() {
        var as = audiojs.createAll();
    });
</script>
<script>
    $(window).on('load',function () {
        $("#loader-container").fadeOut('slow')
    });

    var tipo = '';
    var subtipo = '';
    var centrosArr = [];
    var filtrarProyectos = false;
    var filtrarAliados = false;
    var filtrarMapa = false;
    var optionSelected ="";

    function loading() {
        $("#loader-container").fadeIn('slow')
    }
    function loadingClose() {
        $("#loader-container").fadeOut('slow')
    }
    function getCentros(){
        var input = $("#input-centros").val()
        optionSelected = "los filtros seleccionados"
        $.get('/filter-centros?input='+input+'&tipo='+tipo+'&subtipo='+subtipo,function (centros) {
            $("#list-centros-container").empty();
            $("#centros-result").text(centros.length+' Resultados')
            centrosArr = centros;
            for(var i = 0; i<centros.length; i++){
                if(Number(centros[i].subtipo.categoria_id) === 1) {
                    $("#list-centros-container").append(`<p style="line-height: 90%" class="mb-3"><a href="#" onclick="mostrarModalDetalle('${centros[i].id}','${centros[i].nombre}',true)" class="mysidebar__link mysidebar__link--verde">${centros[i].nombre}</a></p>`)
                }else if(Number(centros[i].subtipo.categoria_id) === 2) {
                    $("#list-centros-container").append(`<p style="line-height: 90%" class="mb-3"><a href="#" onclick="mostrarModalDetalle('${centros[i].id}','${centros[i].nombre}',true)" class="mysidebar__link mysidebar__link--rojo">${centros[i].nombre}</a></p>`)
                }else if(Number(centros[i].subtipo.categoria_id) === 3) {
                    $("#list-centros-container").append(`<p style="line-height: 90%" class="mb-3"><a href="#" onclick="mostrarModalDetalle('${centros[i].id}','${centros[i].nombre}',true)" class="mysidebar__link mysidebar__link--amarillo">${centros[i].nombre}</a></p>`)
                }else if(Number(centros[i].subtipo.categoria_id) === 4) {
                    $("#list-centros-container").append(`<p style="line-height: 90%" class="mb-3"><a href="#" onclick="mostrarModalDetalle('${centros[i].id}','${centros[i].nombre}',true)" class="mysidebar__link mysidebar__link--azul">${centros[i].nombre}</a></p>`)
                }
            }
        })
    }

    function changeTipo(id, divId) {
        /*
        tipo = $(select).val();
        $(".mysidebar__select").each(function(){
            $(this).removeClass('mysidebar__select--verde')
            $(this).removeClass('mysidebar__select--rojo')
            $(this).removeClass('mysidebar__select--amarillo')
            $(this).removeClass('mysidebar__select--azul')
        })
        if(tipo !== "") {
            $.get('/centros/subtipos/' + tipo, function (resp) {
                $("#subtipos").empty();
                $("#subtipos").append(`<option value="">Subtipo</option>`)
                for (var i = 0; i < resp.length; i++) {
                    $("#subtipos").append(`<option value="${resp[i].id}">${resp[i].nombre}</option>`)
                }
            });
            var clase = "mysidebar__select--";
            if(Number(tipo) === 1){
                clase += "verde"
            }else if(Number(tipo) === 2){
                clase += "rojo"
            }else if(Number(tipo) === 3){
                clase += "amarillo"
            }else if(Number(tipo) === 4){
                clase += "azul"
            }
            $(".mysidebar__select").each(function(){
                $(this).addClass(clase)
            })
        }else{
            $("#subtipos").empty();
            $("#subtipos").append(`<option value="">Subtipo</option>`)
        }
         */
        if(!($("#"+divId).hasClass('selected'))){
            closeAllTipos(divId)
            tipo = id;
            subtipo = "";
            $("#"+divId).addClass('selected')
            $.get('/centros/subtipos/' + id, function (resp) {
                $("#"+divId+" .mysidebar__subtipo").empty()
                for (var i = 0; i < resp.length; i++) {
                    $("#"+divId+" .mysidebar__subtipo").append(`<div class="subtipo-item" onclick="changeSubtipo('${resp[i].id}',this)"><span class="line">&#x2015;</span> ${resp[i].nombre}</div>`)
                }
            });
            getCentros()
        }else{
            closeAllTipos()
            tipo = "";
            subtipo = "";
            getCentros()
        }
    }

    function cerrarTipo(divId) {
        if($("#"+divId).hasClass('selected')) {
            closeAllTipos()
            tipo = "";
            subtipo = "";
            getCentros()
        }
    }

    function closeAllTipos(ignoreId = 0) {
        $(".mysidebar__tipo").each((idx,item)=>{
            if(Number($(item).attr('id')) !== Number(ignoreId)) {
                $(item).removeClass('selected')
                var subtipo = $(item).find('.mysidebar__subtipo').first()
                $(subtipo).empty()
            }
        })
    }

    function changeSubtipo(id, div) {
        //subtipo = $(select).val();
        $(".subtipo-item").each((idx,item)=>{
                $(item).removeClass('selected')
        })
        $(div).addClass('selected')
        subtipo = id;
        getCentros()
    }

    function toggleSidebar() {
        $("#sidebar").toggleClass('mysidebar--active')
    }

    function mostrarModalDetalle(centroId,centroNombre, sidebar = false, marker = false) {
        let id = centroId;
        optionSelected = centroNombre;
        if(filtrarProyectos){
            loading();
            let myArr = [{id: id}];
            updateProyectos(myArr)
        }else if(filtrarAliados){
            loading();
            let myArr = [{id: id}];
            updateAliados(myArr)
        }else if(filtrarMapa && (!marker)){
            loading();
            updateMarkers(id)
        }else{
            $("#modal-detalle-centro").load('/centros/detalles/' + id)
            $("#modal-container").show()
            $("body").addClass('scroll-hidden')
        }
        if ((window.matchMedia('(max-width: 992px)').matches) && sidebar) {
            toggleSidebar()
        }
    }

    function mostrarModalDetalleProyecto(id) {
        $("#modal-detalle-centro").load('/centros/detalles/proyecto/'+id)
        $("#modal-container").show()
        $("body").addClass('scroll-hidden')
    }

    function closeModal() {
        $("#modal-detalle-centro").empty()
        $("#modal-container").fadeOut()
        $("body").removeClass('scroll-hidden')
    }
</script>
@yield('scripts')
</body>
</html>
