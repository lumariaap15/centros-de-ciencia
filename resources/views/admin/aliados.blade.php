@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .tr-editable p{
            display: none;
        }
        .tr-editable input{
            display: block !important;
            width: 80%;
        }

        .tr-editable .boton-enviar{
            display: block !important;
        }
        .tr-editable .botones-normales{
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Aliados
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Aliados</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                        <p class="text-danger">{{$errors->first('nombre')}}</p>
                        <p class="text-danger">{{$errors->first('nombre_edit')}}</p>
                        <p class="text-danger">{{$errors->first('url')}}</p>
                        <p class="text-danger">{{$errors->first('url_edit')}}</p>
                        <form class="form" id="form-store-aliadosGeneral" method="post" action="{{route('aliados.store')}}">
                            @csrf
                                <div class="row">
                                    <div class="col-4 pt-1">
                                        <input type="text" name="nombre" class="form-control mr-3"  placeholder="Nombre" required>
                                    </div>
                                    <div class="col-4 pt-1">
                                        <input type="text" name="url" class="form-control" placeholder="URL" required>
                                    </div>
                                    <div class="col-3">
                                        <button id="btn-store-aliadosGeneral" type="button" class="btn btn-primary btn-block btn-sm ml-3">Enviar</button>
                                    </div>
                                </div>




                        </form>
                            </div>
                    </div>
                <div class="card ">
                    <div class="body">
                        <table class="table table-hover table-striped table-borderless m-b-0" id="table-centros">
                            <thead>
                            <tr>

                                <th>Nombre</th>
                                <th>URL</th>
                                <th>Acciones</th>


                            </tr>
                            </thead>
                            <tbody class="no-border-x">
                            @foreach($aliados as $aliado)
                                <tr>

                                    <td>
                                        <p>{{$aliado->nombre}}</p>
                                        <input  type="text" name="nuevoNombre" class="form-control bg-white" value="{{$aliado->nombre}}" style="display: none">
                                    </td>

                                    <td>
                                        <p>{{$aliado->url}}</p>
                                        <input  type="text" name="nuevoURL" class="form-control bg-white" value="{{$aliado->url}}" style="display: none">
                                    </td>
                                    <td>
                                        <div class="botones-normales">
                                            <div class="btn-group">
                                                <button onclick="mostrarInputs(this)" class="btn btn-primary btn-sm">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                <button onclick="eliminarEntrada('{{$aliado->id}}')" class="btn btn-danger btn-sm">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>
                                        </div>
                                        <button onclick="updateEntrada('{{$aliado->id}}')" class="btn btn-success btn-sm boton-enviar" style="display: none">
                                            <i style="font-size: 16px" class="material-icons">check_circle</i>
                                        </button>
                                        <form id="form-entrada-{{$aliado->id}}" action="{{route('aliados.destroy',$aliado->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <form id="form-editar-{{$aliado->id}}" action="{{route('aliados.update',$aliado->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="nombre_edit">
                                            <input type="hidden" name="url_edit">
                                            @method('PUT')
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script>
        $(function () {
            $('table').DataTable();
            $('#btn-store-aliadosGeneral').click(function (e) {
                e.preventDefault();
                cargando();
                $('#form-store-aliadosGeneral').trigger('submit')
            })
        });

        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este aliado?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }

        function updateEntrada(id) {
            cargando()
            var formSelector  = "#form-editar-"
            $("input[name='nombre_edit']").val($("input[name='nuevoNombre']").val());
            $("input[name='url_edit']").val($("input[name='nuevoURL']").val());
            $(formSelector+id).trigger('submit')

        }

        function mostrarInputs(button) {
            var tr = $(button).closest('tr')
            tr.addClass('tr-editable')
        }
    </script>
@endsection

