@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .img-container{
            height: 150px;
            width: 100%;
            background-size: cover;
            background-position: center;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Imagenes del centro {{$centroNombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Imagenes del centro {{$centroId}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                        <p class="text-danger">{{$errors->first()}}</p>


                        <form id="form-store-imagenes" enctype="multipart/form-data" class="form" method="post" action="{{route('imagenes.store')}}">
                            @csrf
                            <div class="row">
                                <div class="col-5 pt-1">
                                    <input type="file" accept="image/*" name="image" class="form-control mr-3"  placeholder="image" required>
                                </div>
                                <div class="col-4 pt-1">
                                    <input type="text" placeholder="fuente" name="url" class="form-control">
                                </div>
                                <div class="col-3">
                                    <button type="button" id="btn-store-imagenes" class="btn btn-primary  btn-sm ml-3 btn-block">Enviar</button>
                                </div>
                            </div>
                            <input type="hidden" name="centroId" value="{{$centroId}}">




                        </form>
                            </div>
                    </div>
                <div class="card ">
                    <div class="body">
                        <div class="row">
                            @foreach($imagenes as $imagen)
                            <div class="col col-12 col-md-6 col-lg-3">
                                <a target="_blank" href="{{\Illuminate\Support\Facades\Storage::url($imagen->image)}}">
                                <div class="img-container" style="background-image: url('{{\Illuminate\Support\Facades\Storage::url($imagen->image)}}')">
                                </div>
                                </a>
                                <p class="text-center">Fuente: {{$imagen->url}} <a href="javascript:void(0)"  onclick="editarFuente('{{$imagen->id}}','{{$imagen->url}}')" class="text-info"><i class="material-icons">edit</i></a></p>

                                <button onclick="eliminarEntrada({{$imagen->id}})" class="btn btn-danger btn-block mt-2">Eliminar</button>
                                <form  id="form-entrada-{{$imagen->id}}" action="{{route('imagenes.destroy',$imagen->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                </form>
                            </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script>
        $('#btn-store-imagenes').click(function (e) {
            e.preventDefault();
            cargando();
            $('#form-store-imagenes').trigger('submit')
        })

        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar esta imagen?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }

        function editarFuente(id,valorInicial) {
            swal({
                title: 'Cambiar Fuente',
                input: 'text',
                inputValue: valorInicial,
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Enviar',
                showLoaderOnConfirm: true,
            }).then((value) => {
                $.ajax({
                    type: "POST",
                    url: '/admin/centros/imagenes/'+id,
                    data: {url: value,
                            _token:"{{csrf_token()}}", _method:'PUT' },
                    beforeSend: function () {
                    },
                    success: function (result) {
                        swal({
                            title:'¡Bien!',
                            text:result.message,
                            type:'success',
                        }).then(()=>{
                            window.location.reload()
                        });

                    },
            });

        })

        }

    </script>
@endsection

