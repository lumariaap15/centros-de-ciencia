@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .tr-editable p{
            display: none;
        }
        .tr-editable input{
            display: block !important;
        }

        .tr-editable .boton-enviar{
            display: block !important;
        }
        .tr-editable .botones-normales{
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Entradas del centro {{$centroNombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Entradas del centro {{$centroId}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                            <p class="text-danger">{{$errors->first('nombre')}}</p>
                            <p class="text-danger">{{$errors->first('nombre_edit')}}</p>
                            <p class="text-danger">{{$errors->first('valor')}}</p>
                            <p class="text-danger">{{$errors->first('valor_edit')}}</p>

                        <form method="post" action="{{route('entradas.store')}}" id="form-store-entradas">
                            @csrf

                                <div class="row">
                                    <div class="col-4 pt-1">
                                        <input type="number" name="valor"  class="form-control mr-3"  placeholder="Valor">
                                    </div>
                                    <div class="col-4 pt-1">
                                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" required>
                                    </div>
                                    <div class="col-4">
                                        <button type="button" id="btn-store-entradas" class="btn btn-primary  btn-sm btn-block">Enviar</button>
                                    </div>
                                </div>







                            <input type="hidden" name="centroId" value="{{$centroId}}">
                        </form>
                            </div>
                    </div>
                <div class="card ">
                    <div class="body">
                        <table class="table table-hover table-striped table-borderless m-b-0" id="table-centros">
                            <thead>
                            <tr>

                                <th>Valor</th>
                                <th>Nombre</th>
                                <th>Acciones</th>


                            </tr>
                            </thead>
                            <tbody class="no-border-x">
                            @foreach($entradas as $entrada)
                                <tr>

                                    <td>

                                        <p class="money">${{number_format($entrada->valor,0,",",".")}}</p>
                                        <input type="number" id="nuevoValor-{{$entrada->id}}" class="form-control bg-white" value="{{$entrada->valor}}" style="display: none">
                                    </td>

                                    <td>
                                        <p>{{$entrada->nombre}}</p>
                                        <input id="nuevoNombre-{{$entrada->id}}" class="form-control bg-white" value="{{$entrada->nombre}}" style="display: none">
                                    </td>
                                    <td>
                                        <div class="botones-normales">
                                            <div class="btn-group">
                                                <button onclick="mostrarInputs(this)" class="btn btn-primary btn-sm">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                <button onclick="eliminarEntrada('{{$entrada->id}}')" class="btn btn-danger btn-sm">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>

                                        </div>
                                        <button onclick="updateEntrada('{{$entrada->id}}')" class="btn btn-success btn-sm boton-enviar" style="display: none">
                                            <i style="font-size: 16px" class="material-icons">check_circle</i>
                                        </button>

                                        <form id="form-entrada-{{$entrada->id}}" action="{{route('entradas.destroy',$entrada->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <form id="form-editar-{{$entrada->id}}" action="{{route('entradas.update',$entrada->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="valor_edit">
                                            <input type="hidden" name="nombre_edit">
                                            @method('PUT')
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>

    <script>
        $(function () {
            //$('table').DataTable();
            $('#btn-store-entradas').click(function (e) {
                e.preventDefault();
                cargando();
                $('#form-store-entradas').trigger('submit')
            })

        });

        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar esta entrada?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=> {
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }

        function updateEntrada(id) {
            cargando()
            var formSelector  = "#form-editar-"
            var nuevoNombre= "#nuevoNombre-"+id
            var nuevoValor = "#nuevoValor-"+id
            $("input[name='valor_edit']").val($(nuevoValor).val());
            $("input[name='nombre_edit']").val($(nuevoNombre).val());
            $(formSelector+id).trigger('submit')

        }

        function mostrarInputs(button) {
            var tr = $(button).closest('tr')
            tr.addClass('tr-editable')
        }


    </script>
@endsection

