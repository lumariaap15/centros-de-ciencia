@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .tr-editable p{
            display: none;
        }
        .tr-editable textarea{
            display: block !important;
        }

        .tr-editable .boton-enviar{
            display: block !important;
        }
        .tr-editable .botones-normales{
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Oferta General del centro {{$centroNombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Oferta General {{$centroId}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                        @if($errors->has('descripcion'))
                            <div class="alert alert-danger" role="alert">
                                {{$errors->first('descripcion')}}
                            </div>
                        @endif
                        @if($errors->has('descripcion_edit'))
                            <div class="alert alert-danger" role="alert">
                                {{$errors->first('descripcion')}}
                            </div>
                        @endif
                        <form id="form-store-servicios" class="form" method="post" action="{{route('servicios.store')}}">
                            @csrf
                            <input type="hidden" name="centroId" value="{{$centroId}}">
                            <label for="descripcion">Descripcion</label>

                            <textarea form="form-store-servicios" name="descripcion" id="descripcion" class="form-control" placeholder="Ingrese aqui..."></textarea>
                            <div class="row justify-content-center">
                                <button type="button" id="btn-store-servicios" class="btn btn-primary btn-sm mt-3">Enviar</button>

                            </div>



                        </form>
                    </div>
                </div>
                <div class="card ">
                    <div class="body">
                        <table class="table table-hover table-striped table-borderless m-b-0" id="table-centros">
                            <thead>
                            <tr>

                                <th>Descripcion</th>
                                <th>Acciones</th>


                            </tr>
                            </thead>
                            <tbody class="no-border-x">
                            @foreach($servicios as $servicio)
                                <tr>

                                    <td>
                                        <p>{{$servicio->descripcion}}</p>
                                        <textarea id="nuevaDescripcion-{{$servicio->id}}" class="form-control bg-white" style="display: none" >{{$servicio->descripcion}}</textarea>
                                    </td>

                                    <td>

                                        <div class="botones-normales">
                                            <div class="btn-group btn-group-sm" role="group" >
                                                <button onclick="mostrarInputs(this)" class="btn btn-primary btn-sm">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                <button onclick="eliminarEntrada('{{$servicio->id}}')" class="btn btn-danger btn-sm">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>
                                        </div>
                                        <button onclick="updateEntrada('{{$servicio->id}}')" class="btn btn-success btn-sm boton-enviar" style="display: none">
                                            <i class="material-icons">check_circle</i>
                                        </button>
                                        <form id="form-entrada-{{$servicio->id}}" action="{{route('servicios.destroy',$servicio->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <form id="form-editar-{{$servicio->id}}" action="{{route('servicios.update',$servicio->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="descripcion_edit">

                                            @method('PUT')
                                        </form>

                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(function () {
            //$('table').DataTable();
            $('#btn-store-servicios').click(function (e) {
                e.preventDefault();
                cargando();
                $('#form-store-servicios').trigger('submit')
            })

        });
        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este servicio?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }

        function updateEntrada(id) {
            var formSelector  = "#form-editar-"
            var nuevaDescripcion = "#nuevaDescripcion-"+id
            $("input[name='descripcion_edit']").val($(nuevaDescripcion).val());
            $(formSelector+id).trigger('submit')



        }

        function mostrarInputs(button) {
            var tr = $(button).closest('tr')
            tr.addClass('tr-editable')
        }
    </script>
@endsection

