@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Centros de Ciencia
                    <small>Listado de los centros de ciencia en Colombia</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}}"><i class="zmdi zmdi-home"></i> Centros</a></li>

                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card ">
                    <div class="body table-responsive">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                            <div class = "row justify-content-end">
                                <a href="{{route('centros.create')}}" class="btn btn-primary btn-sm ">
                                    <i class="material-icons">add_circle</i>
                                    <span style="vertical-align: top" class="d-inline-block pt-1">Agregar Centro</span>
                                </a></div>

                            <table class="table table-hover table-striped table-borderless m-b-0" id="table-centros">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Subtipo</th>
                                <th>Departamento</th>
                                <th>Municipio</th>
                                <th>Dirección</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody class="no-border-x">
                            @foreach($centros as $centro)
                                <tr>
                                    <td>{{$centro->id}}</td>
                                    <td>{{$centro->nombre}}</td>
                                    <td>{{$centro->subtipo->padre->nombre}}</td>
                                    <td>{{$centro->subtipo->nombre}}</td>
                                    <td>{{$centro->departamento->descripcion}}</td>
                                    <td>{{$centro->municipio->descripcion}}</td>
                                    <td>{{$centro->direccion}}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" >
                                        <a href="{{route('centros.edit',$centro->id)}}" data-toggle="tooltip"  title="editar" class="btn btn-primary btn-sm">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a href="{{route('entradas.index',$centro->id)}}"  data-toggle="tooltip" title="entradas" class="btn btn-primary btn-sm">
                                            <i class="material-icons">directions_run</i>
                                        </a>
                                        <a href="{{route('servicios.index',$centro->id)}}" data-toggle="tooltip"  title="oferta general" class="btn btn-primary btn-sm">
                                            <i class="material-icons">assignment</i>
                                        </a>
                                            <a href="{{route('proyectos.index',$centro->id)}}" data-toggle="tooltip"  title="proyectos" class="btn btn-primary btn-sm">
                                                <i class="material-icons">folder</i>
                                            </a>
                                        <a href="{{route('centros.aliados',$centro->id)}}"  data-toggle="tooltip"  title="aliados" class="btn btn-primary btn-sm">
                                            <i class="material-icons">contacts</i>
                                        </a>
                                        <a href="{{route('horarios.index',$centro->id)}}"  data-toggle="tooltip"  title="horarios" class="btn btn-primary btn-sm">
                                            <i class="material-icons">schedule</i>
                                        </a>
                                            <a href="{{route('imagenes.index',$centro->id)}}" data-toggle="tooltip"  title="imagenes" class="btn btn-primary btn-sm">
                                                <i class="material-icons">burst_mode</i>
                                            </a>



                                            <button class="btn btn-danger btn-sm"  data-toggle="tooltip"  title="eliminar" onclick="eliminarEntrada('{{$centro->id}}')"><i class="material-icons">delete</i></button>
                                        <form id="form-entrada-{{$centro->id}}"  method="post" action="{{route('centros.destroy',$centro->id)}}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script>
        $(function () {
            $('#table-centros').DataTable();
            $('[data-toggle="tooltip"]').tooltip()
        });
        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este centro?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                $("#form-entrada-"+id).trigger('submit')
            });
        }
    </script>
@endsection
