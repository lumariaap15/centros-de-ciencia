@extends('admin.layout')

@section('styles')
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Crear un centro de ciencia
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                            <p class="text-danger">{{$errors->first('nombre')}}</p>
                            <p class="text-danger">{{$errors->first('descripcion')}}</p>
                            <p class="text-danger">{{$errors->first('latitud')}}</p>
                            <p class="text-danger">{{$errors->first('longitud')}}</p>
                        <form id="create-form" >
                            @csrf
                            <div class="form-group">
                                <label for="nombre">Nombre*</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" required>
                            </div>
                            <div class="form-group">
                                <label for="tipo">Tipo*</label>
                                <select class="form-control show-tick" id="tipo" name="tipo" onchange="getSubtipos(this)">
                                    @foreach($categoriasPadre as $cP)
                                        <option value="{{$cP->id}}" >{{$cP->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="subtipo">Subtipo*</label>
                                <select class="form-control show-tick" id="subtipo" name="categoria_id">
                                    @foreach($categoriasHijas as $cH)
                                        <option value="{{$cH->id}}" >{{$cH->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="departamento">Departamento*</label>
                                <select class="form-control show-tick" id="departamento" name="departamento_codigo" onchange="getMunicipios(this)">
                                    <option value="">Selecciona un departamento...</option>
                                    @foreach($departamentos as $departamento)
                                        <option value="{{$departamento->codigo}}" >{{$departamento->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="municipios">Municipio*</label>
                                <select class="form-control show-tick" id="municipios" name="municipio_codigo">
                                    <option value="">Selecciona un municipio...</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="owner">Dueño</label>
                                <input type="text" name="owner" class="form-control" id="owner" >
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea name="descripcion" id="descripcion" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="direccion">Dirección*</label>
                                <input type="text" name="direccion" class="form-control" id="direccion"  required>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="latitud">Latitud*</label>
                                        <input type="text" name="latitud" class="form-control" id="latitud"  required>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="longitud">Longitud*</label>
                                        <input type="text" name="longitud" class="form-control" id="longitud" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pagina_web">Página web</label>
                                <input type="text" name="pagina_web" class="form-control" id="pagina_web" >
                            </div>
                            <div class="form-group">
                                <label for="correo">Correo</label>
                                <input type="text" name="correo" class="form-control" id="correo" >
                            </div>
                            <div class="row">
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="telefono">Teléfono</label>
                                        <input type="text" name="telefono" class="form-control" id="telefono" >
                                    </div>
                                </div>
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="celular">Celular</label>
                                        <input type="text" name="celular" class="form-control" id="celular" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fecha_creacion">Fecha de creación</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" name="fecha_creacion" class="form-control " id="date" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="audio">Audio</label>
                                <input type="file" accept="audio/*" name="audio" class="form-control" id="audio" >
                            </div>
                            <div class="row justify-content-center">
                                <a href="{{route('centros.index')}}" class="btn btn-secondary">Volver</a>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script>

        $(function () {
            $("#create-form").submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var formData = new FormData(form[0]);

                console.log($('#audio').val())
                if($('#audio').val()!="") {
                    formData.append('audio', $('#audio')[0].files[0]);
                }


                console.log(formData.values());

                $.ajax({
                    type: "POST",
                    url: '{{route("centros.store")}}',
                    data: formData,
                    processData : false,
                    contentType : false,
                    beforeSend: function () {
                        cargando();
                    },
                    success: function (result) {
                        cerrarLoader()
                        swal({
                            title:'¡Bien!',
                            text:result.message,
                            type:'success',
                        }).then(()=>{
                            window.location.href = '{{route('centros.index')}}'
                        });

                        form.reset();
                    },
                    error: function (xhr, status) {
                        cerrarLoader()
                        swal({
                            title:'¡Error!',
                            text:xhr.responseJSON,
                            type:'error',
                        })
                    },
                    complete: function (xhr, status) {
                        //fincarga();
                    }
                });

            });


        })
        $('#date').bootstrapMaterialDatePicker({ weekStart : 0, time: false, lang:'es'});

        function getSubtipos(select) {
            var tipoId = $(select).val()
            $.get('/centros/subtipos/'+tipoId, function (resp) {
                $("#subtipo").empty()

                for(var i=0; i<resp.length; i++){
                    $("#subtipo").append(`<option value="${resp[i].id}">${resp[i].nombre}</option>`)
                }

                $('#subtipo').selectpicker('refresh');

            })
        }
        function getMunicipios(select) {
            var departamentoId = $(select).val()
            if(departamentoId === '' || departamentoId=== null) {
                $("#municipios").empty()
                $("#municipios").append(`<option value="">Selecciona un municipio...</option>`)
                $('#municipios').selectpicker('refresh');
            }else {
                $.get('/centros/departamentos/' + departamentoId, function (resp) {
                    $("#municipios").empty()

                    for (var i = 0; i < resp.length; i++) {
                        $("#municipios").append(`<option value="${resp[i].codigo}">${resp[i].descripcion}</option>`)
                    }

                    $('#municipios').selectpicker('refresh');

                })
            }
        }

    </script>
@endsection

