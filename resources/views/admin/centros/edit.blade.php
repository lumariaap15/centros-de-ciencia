@extends('admin.layout')

@section('styles')
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />

@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Centro de ciencia - {{$centro->nombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">{{$centro->id}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">

                        <form id="editForm">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" value="{{$centro->nombre}}" required>
                            </div>
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="form-control show-tick" id="tipo" name="tipo" onchange="getSubtipos(this)">
                                    @foreach($categoriasPadre as $cP)
                                        <option value="{{$cP->id}}" {{$cP->id === $centro->subtipo->categoria_id ? 'selected' : ''}}>{{$cP->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="subtipo">Subtipo</label>
                                <select class="form-control show-tick" id="subtipo" name="categoria_id">
                                    @foreach($categoriasHijas as $cH)
                                        <option value="{{$cH->id}}" {{$cH->id === $centro->categoria_id ? 'selected' : ''}}>{{$cH->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="departamento">Departamento*</label>
                                <select class="form-control show-tick" id="departamento" name="departamento_codigo" onchange="getMunicipios(this)">
                                    @foreach($departamentos as $departamento)
                                        <option value="{{$departamento->codigo}}" {{intval($departamento->codigo) === intval($centro->departamento_codigo) ? 'selected' : ''}}>{{$departamento->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="municipios">Municipio*</label>
                                <select class="form-control show-tick" id="municipios" name="municipio_codigo">
                                    @foreach($municipios as $municipio)
                                        <option value="{{$municipio->codigo}}" {{intval($municipio->codigo) === intval($centro->municipio_codigo) ? 'selected' : ''}}>{{$municipio->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="owner">Dueño</label>
                                <input type="text" name="owner" class="form-control" id="owner" value="{{$centro->owner}}">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea name="descripcion" id="descripcion" class="form-control">
                                    {{$centro->descripcion}}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="direccion">Dirección</label>
                                <input type="text" name="direccion" class="form-control" id="direccion" value="{{$centro->direccion}}" required>
                            </div>
                            <div class="row">
                               <div class="col col-12 col-md-6 px-1">
                                   <div class="form-group">
                                       <label for="latitud">Latitud</label>
                                       <input type="text" name="latitud" class="form-control" id="latitud" value="{{$centro->latitud}}" required>
                                   </div>
                               </div>
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="longitud">Longitud</label>
                                        <input type="text" name="longitud" class="form-control" id="longitud" value="{{$centro->longitud}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pagina_web">Página web</label>
                                <input type="text" name="pagina_web" class="form-control" id="pagina_web" value="{{$centro->pagina_web}}">
                            </div>
                            <div class="form-group">
                                <label for="correo">Correo</label>
                                <input type="text" name="correo" class="form-control" id="correo" value="{{$centro->correo}}">
                            </div>
                            <div class="row">
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="telefono">Teléfono</label>
                                        <input type="text" name="telefono" class="form-control" id="telefono" value="{{$centro->telefono}}">
                                    </div>
                                </div>
                                <div class="col col-12 col-md-6 px-1">
                                    <div class="form-group">
                                        <label for="celular">Celular</label>
                                        <input type="text" name="celular" class="form-control" id="celular" value="{{$centro->celular}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fecha_creacion">Fecha de creación</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" name="fecha_creacion" class="form-control " id="date" value="{{$centro->fecha_creacion}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="audio">Cambiar Audio</label>
                                        <input type="file" accept="audio/*" name="audio" class="form-control" id="audio" >
                                    </div>
                                </div>
                                @if($centro->audio!=null)
                                    <div class="col col-12 col-md-6">
                                        <label>Audio actual</label>
                                        <div class="d-flex mb-3">
                                            <audio controls>
                                                <source src="{{\Illuminate\Support\Facades\Storage::url($centro->audio)}}">
                                            </audio>
                                            <button  type="button" onclick="eliminarEntrada('{{$centro->id}}')" class="btn btn-danger ml-2 rounded-circle"><i class="material-icons">delete</i></button>

                                        </div>
                                    </div>
                                @endif
                            </div>


                            <div class="row justify-content-center">
                                <a href="{{route('centros.index')}}" class="btn btn-secondary">Volver</a>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </form>
                        <form  id="form-entrada-{{$centro->id}}" action="{{route('centros.destroyAudio',$centro->id)}}" method="post">
                            @csrf
                            @method('DELETE')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>


    <script>
        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este audio?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                console.log(("#form-entrada-"+id))
                $("#form-entrada-"+id).trigger('submit')
            });
        }

        function getMunicipios(select) {
            var departamentoId = $(select).val()
            if(departamentoId === '' || departamentoId=== null) {
                $("#municipios").empty()
                $("#municipios").append(`<option value="">Selecciona un municipio...</option>`)
                $('#municipios').selectpicker('refresh');
            }else {
                $.get('/centros/departamentos/' + departamentoId, function (resp) {
                    $("#municipios").empty()

                    for (var i = 0; i < resp.length; i++) {
                        $("#municipios").append(`<option value="${resp[i].codigo}">${resp[i].descripcion}</option>`)
                    }

                    $('#municipios').selectpicker('refresh');

                })
            }
        }

        $(function () {
            $('#editForm').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var formData = new FormData(form[0]);
                /*
                console.log(form[0])
                var formData = new FormData(form[0]);
                console.log(formData.keys())


                if($('#audio').val()!=="") {
                    formData.append('audio', $('#audio')[0].files[0]);
                }

                console.log(formData.keys())

                 */

                $.ajax({
                    type: "POST",
                    url: '{{route("centros.update",$centro->id)}}',
                    data: formData,
                   processData : false,
                   contentType : false,
                    beforeSend: function () {
                        //cargando();
                    },
                    success: function (result) {
                        swal({
                            title:'¡Bien!',
                            text:result.message,
                            type:'success',
                        }).then(()=>{
                            window.location.reload()
                        });

                    },
                    error: function (xhr, status) {
                        console.log(xhr)
                        responseError(xhr);
                    },
                    complete: function (xhr, status) {
                        //fincarga();
                    }
                });

            });

            $('#date').bootstrapMaterialDatePicker({ weekStart : 0, time: false, lang:'es'});



        })

        function getSubtipos(select) {
            var tipoId = $(select).val()
            $.get('/centros/subtipos/'+tipoId, function (resp) {
                $("#subtipo").empty()

               for(var i=0; i<resp.length; i++){
                   $("#subtipo").append(`<option value="${resp[i].id}">${resp[i].nombre}</option>`)
               }

                $('#subtipo').selectpicker('refresh');

            })
        }

    </script>
@endsection

