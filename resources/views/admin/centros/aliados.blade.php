@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .tr-editable p{
            display: none;
        }
        .tr-editable input{
            display: block !important;
        }

        .tr-editable .boton-enviar{
            display: block !important;
        }
        .tr-editable .botones-normales{
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Aliados del centro {{$centroNombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Aliados {{$centroId}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                        <form id="form-store-aliados" action="{{route('centros.asociarAliados',$centroId)}}" method="post">
                            @csrf
                        <div class="row">
                                <div class="col col-md-8">
                                    <input id="aliados-autocomplete" class="form-control" type="text" placeholder="Aliado">
                                    <input id="selected-aliado" class="form-control" type="hidden" name="aliado_id">
                                </div>
                                <div class="col col-md-4">
                                    <button class="btn btn-primary btn-block btn-sm " id="btn-store-aliados" type="button">Agregar</button>
                                </div>
                        </div>

                        </form>
                            <table class="table table-hover table-striped table-borderless m-b-0" >
                                <thead>
                                <tr>

                                    <th>Nombre</th>
                                    <th>Acciones</th>


                                </tr>
                                </thead>
                                <tbody class="no-border-x">
                        @foreach($aliados as $aliado)
                            <tr>

                                <td>
                                    <p>{{$aliado->nombre}}</p>
                                </td>

                                <td>
                                    <div class="botones-normales">

                                        <button onclick="eliminarEntrada('{{$aliado->id}}')" class="btn btn-danger btn-sm">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </div>
                                    <form id="form-entrada-{{$aliado->id}}" action="{{route('centros.borrarAliados',[$centroId,$aliado->id])}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>

                                </td>

                            </tr>
                        @endforeach

                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        var aliados = [];

        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este aliado?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }


        $(function(){
            $.get('/get-aliados/{{$centroId}}',function (resp) {
                console.log(resp);

               for (x=0 ; x<resp.length ; x++){
                   aliados[x]={label: resp[x].nombre , value:resp[x].id}
               };
               });
            $("#aliados-autocomplete").autocomplete({
                    source:aliados
            });
            $('#btn-store-aliados').click(function (e) {
                e.preventDefault();
                cargando();
                $('#form-store-aliados').trigger('submit')
            })
        });




        $("#aliados-autocomplete").autocomplete({
            /* snip */
            select: function(event, ui) {
                event.preventDefault();
                $("#aliados-autocomplete").val(ui.item.label);
                $("#selected-aliado").val(ui.item.value);

            }
        });


    </script>
@endsection

