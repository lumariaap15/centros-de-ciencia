@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .tr-editable p{
            display: none;
        }
        .tr-editable input{
            display: block !important;
        }

        .tr-editable .boton-enviar{
            display: block !important;
        }
        .tr-editable .botones-normales{
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Horarios del centro {{$centroNombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Horarios del centro {{$centroId}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                        <p class="text-danger">{{$errors->first('horario')}}</p>
                        <p class="text-danger">{{$errors->first('horario_edit')}}</p>

                        <form id="form-store-horarios" method="post" action="{{route('horarios.store')}}">
                            @csrf
                            <input type="hidden" name="centroId" value="{{$centroId}}">

                            <div class="row">
                                <div class="col-8 pt-1">
                                    <input type="text" name="horario" class="form-control mr-3"  placeholder="Ej: Todos los días, de 9:00 a. m. a 4:30 p. m." required>
                                </div>
                                <div class="col-4">
                                    <button type="button" id="btn-store-horarios" class="btn btn-primary  btn-sm ml-3 btn-block">Enviar</button>
                                </div>
                            </div>


                        </form>
                            </div>
                    </div>
                <div class="card ">
                    <div class="body">
                        <table class="table table-hover table-striped table-borderless m-b-0" id="table-centros">
                            <thead>
                            <tr>

                                <th>Dias</th>
                                <th>Acciones</th>


                            </tr>
                            </thead>
                            <tbody class="no-border-x">

                            @foreach($horarios as $horario)
                                <tr>

                                    <td>
                                        <p>{{$horario->horario}}</p>
                                        <input id="nuevoHorario-{{$horario->id}}" class="form-control bg-white" value="{{$horario->horario}}" style="display: none">
                                    </td>

                                    <td>
                                        <div class="botones-normales">
                                            <div class="btn-group">
                                                <button onclick="mostrarInputs(this)" class="btn btn-primary btn-sm">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                <button onclick="eliminarEntrada('{{$horario->id}}')" class="btn btn-danger btn-sm">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>

                                        </div>
                                        <button onclick="updateEntrada('{{$horario->id}}')" class="btn btn-success btn-sm boton-enviar" style="display: none">
                                            <i style="font-size: 16px"  class="material-icons">check_circle</i>
                                        </button>
                                        <form id="form-entrada-{{$horario->id}}" action="{{route('horarios.destroy',$horario->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <form id="form-editar-{{$horario->id}}" action="{{route('horarios.update',$horario->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="horario_edit">
                                            @method('PUT')
                                        </form>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script>
        $(function () {
            //$('table').DataTable();
            $('#btn-store-horarios').click(function (e) {
                e.preventDefault();
                cargando();
                $('#form-store-horarios').trigger('submit')
            })

        });

        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este horario?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }

        function updateEntrada(id) {
            cargando()
            var formSelector  = "#form-editar-";
            var horarioSeleccionado = "#nuevoHorario-"+id;
            $("input[name='horario_edit']").val($(horarioSeleccionado).val());
            $(formSelector+id).trigger('submit')

        }

        function mostrarInputs(button) {
            var tr = $(button).closest('tr')
            tr.addClass('tr-editable')
        }
    </script>
@endsection

