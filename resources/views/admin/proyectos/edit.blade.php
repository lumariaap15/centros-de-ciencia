@extends('admin.layout')

@section('styles')
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('proyectos.index',$centro->id)}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Editar proyecto {{$proyecto->nombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('proyectos.index',$centroId)}}">Proyectos del centro {{$centroId}}</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-6">
                <div class="card ">
                    <div class="body">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif
                            @if($errors->has('nombre'))
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first('nombre')}}
                                </div>
                            @endif
                        <form action="{{route('proyectos.update',$proyecto->id)}}" id="form-editar-proyectos" method="post" >
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="centroId" value="{{$centroId}}">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control mr-3 mb-3"  placeholder="nombre" id="nombre" required value="{{$proyecto->nombre}}">
                            <label for="descripcion">Descripcion</label>
                            <textarea name="descripcion" class="form-control" rows="3" >{{$proyecto->descripcion}}</textarea>
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="form-control show-tick" id="tipo" name="tipo" onchange="getSubtipos(this)">
                                    @foreach($categoriasPadre as $cP)
                                        <option value="{{$cP->id}}" {{$cP->id === $proyecto->subtipo->categoria_id ? 'selected' : ''}} >{{$cP->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="subtipo">Subtipo</label>
                                <select class="form-control show-tick" id="subtipo" name="categoria_id">
                                    @foreach($categoriasHijas as $cH)
                                        <option value="{{$cH->id}}" {{$cH->id === $proyecto->categoria_id ? 'selected' : ''}}>{{$cH->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="checkbox">

                                <input type="checkbox"  name="ASCTEL" id="exampleCheck1" {{$proyecto->ASCTEL === 1 ? 'checked' : ''}}>
                                <label for="exampleCheck1">Estrategia Nacional de Apropiación Social de la Ciencia, Tecnología e Innovación (ASCTeI)</label>
                            </div>

                            <button type="button" id="btn-editar-proyectos" class="btn btn-primary  btn-sm ml-3">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script>
        $(function () {
            //$('table').DataTable();
            $('#btn-editar-proyectos').click(function (e) {
                e.preventDefault();
                cargando();
                $('#form-editar-proyectos').trigger('submit')
            })

        });
        function getSubtipos(select) {
            var tipoId = $(select).val()
            $.get('/centros/subtipos/'+tipoId, function (resp) {
                $("#subtipo").empty()

                for(var i=0; i<resp.length; i++){
                    $("#subtipo").append(`<option value="${resp[i].id}">${resp[i].nombre}</option>`)
                }

                $('#subtipo').selectpicker('refresh');

            })
        }
    </script>
@endsection
