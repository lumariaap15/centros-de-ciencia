@extends('admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .tr-editable p{
            display: none;
        }
        .tr-editable input{
            display: block !important;
        }

        .tr-editable .boton-enviar{
            display: block !important;
        }
        .tr-editable .botones-normales{
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center">
                <a href="{{route('centros.index')}}" class="btn btn-primary btn-sm ">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="pl-3">Proyectos del centro {{$centroNombre}}
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('centros.index')}}"><i class="zmdi zmdi-home"></i> Centros</a></li>
                    <li class="breadcrumb-item active">Proyectos del centro {{$centroId}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix justify-content-center">
            <div class="col col-12 col-md-8">
                <div class="card ">
                    <div class="body">


                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </div>
                        @endif

                            <div class = "row justify-content-end">
                                <a href="{{route('proyectos.create',$centroId)}}" class="btn btn-primary btn-sm ">
                                    <i class="material-icons">add_circle</i>
                                    <span style="vertical-align: top" class="d-inline-block pt-1">Agregar Proyecto</span>
                                </a></div>
                        <table class="table table-hover table-striped table-borderless m-b-0" id="table-centros">
                            <thead>
                            <tr>

                                <th>Nombre</th>
                                <th>Acciones</th>

                            </tr>
                            </thead>
                            <tbody class="no-border-x">
                            @foreach($proyectos as $proyecto)
                                <tr>

                                    <td>
                                        <p>{{$proyecto->nombre}}</p>
                                    </td>

                                    <td>
                                        <div class="botones-normales">
                                            <div class="btn-group">
                                                <a href="{{route('proyectos.edit',$proyecto->id)}}" class="btn btn-primary btn-sm">
                                                    <i class="material-icons">edit</i>
                                                </a>
                                                <button onclick="eliminarEntrada('{{$proyecto->id}}')" class="btn btn-danger btn-sm">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>

                                        </div>

                                        <form id="form-entrada-{{$proyecto->id}}" action="{{route('proyectos.destroy',$proyecto->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(function () {
            $('table').DataTable();
        });

        function eliminarEntrada(id) {
            swal({
                title:'¿Estás seguro?',
                text:'¿Deseas eliminar este proyecto?',
                type:'warning',
                showCancelButton: true,
                cancelButtonText:
                    'Cancelar',
            }).then(()=>{
                cargando()
                $("#form-entrada-"+id).trigger('submit')
            });
        }




    </script>
@endsection

