<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentrosEntrada extends Model
{
    protected $fillable = [
        "nombre",
        "valor",

    ];
}
