<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $primaryKey = 'codigo';
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }
}
