<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    public function subtipo()
    {
        return $this->hasOne(CentrosCategoria::class,'id','categoria_id');
    }

    public function centro()
    {
        return $this->belongsTo(Centro::class);
    }
    protected $fillable = [
        "nombre",
        "ASCTEL",
        "descripcion",
        "categoria_id",


    ];
}
