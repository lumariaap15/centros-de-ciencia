<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $primaryKey = 'codigo';
    public function municipio(){
        return $this->hasMany(Municipio::class);
    }
}
