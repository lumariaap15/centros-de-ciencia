<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentrosHorario extends Model
{
    protected $fillable =[
        'horario'
    ];
}
