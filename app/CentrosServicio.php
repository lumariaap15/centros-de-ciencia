<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentrosServicio extends Model
{
    protected $fillable = [
        "descripcion",
    ];
}
