<?php

namespace App\Http\Controllers;

use App\Centro;
use App\CentrosCategoria;
use App\Http\Requests\ProyectosRequest;
use App\Proyecto;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($centroId)
    {
        $proyectos = Proyecto::query()->where('centro_id',$centroId)->get();
        $centroNombre = Centro::findOrFail($centroId)->nombre;
        return view('admin.proyectos.index',compact('proyectos','centroNombre', 'centroId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($centroId)
    {
        $proyectos = Proyecto::query()->where('centro_id',$centroId)->get();
        $centroNombre = Centro::findOrFail($centroId)->nombre;
        $centro = Centro::query()->findOrFail($centroId);
        $categoriasPadre = CentrosCategoria::query()->where('categoria_id',null)->get();
        $categoriasHijas = CentrosCategoria::query()->where('categoria_id',$centro->subtipo->categoria_id)->get();

        return view('admin.proyectos.crear',compact('proyectos','centroNombre','centroId','centroId','centro','categoriasPadre','categoriasHijas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectosRequest $request)
    {

        try{

            $proyecto = new Proyecto();
            $proyecto->fill($request->all());
            $proyecto->centro_id = $request->centroId;
            if(!$request->has('ASCTEL')){
                $proyecto->ASCTEL= 0;
            }else{
                $proyecto->ASCTEL= 1;
            }
            $proyecto->save();
            return redirect()->route('proyectos.index',$proyecto->centro_id )->with('success','El proyecto se agregó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $proyecto = Proyecto::query()->findOrFail($id);

        $centro = Centro::query()->findOrFail($proyecto->centro_id);
        $centroId = $centro->id;
        $centroNombre = $centro -> nombre;
        $categoriasPadre = CentrosCategoria::query()->where('categoria_id',null)->get();
        $categoriasHijas = CentrosCategoria::query()->where('categoria_id',$centro->subtipo->categoria_id)->get();

        return view('admin.proyectos.edit',compact('proyecto','centroNombre','centroId','centro','categoriasPadre','categoriasHijas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProyectosRequest $request, $id)
    {
        try{

            $proyecto = Proyecto::findOrFail($id);
            $proyecto->fill($request->all());
            $proyecto->centro_id = $request->centroId;
            if(!$request->has('ASCTEL')){
                $proyecto->ASCTEL= 0;
            }else{
                $proyecto->ASCTEL= 1;
            }
            $proyecto->save();
            return redirect()->route('proyectos.index',$proyecto->centro_id )->with('success','El proyecto se editó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $servicio = Proyecto::findOrFail($id);

            $servicio->delete();
            return redirect()->back()->with('success','El proyecto se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
