<?php

namespace App\Http\Controllers;

use App\Centro;
use App\CentroImage;
use App\Http\Requests\ImagenesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Exception;

class ImagenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($centroId)
    {
        $imagenes = CentroImage::query()->where('centro_id',$centroId)->get();
        $centroNombre = Centro::findOrFail($centroId)->nombre;
        return view('admin.imagenes',compact('centroNombre','centroId','imagenes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    try{
        $v = Validator::make($request->all(),[
            'image'=>'required'
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors('Seleccione un archivo para subir');
        }
    $image = Storage::put('public',$request->image);
    $centroImagen = new CentroImage();
    $centroImagen->image = $image;
    $centroImagen->url= $request->url;
    $centroImagen->centro_id = $request->centroId;
    $centroImagen->save();
        return redirect()->route('imagenes.index',$centroImagen->centro_id )->with('success','La imagen se agregó correctamente.');
    }catch (\Exception $e){
        return redirect()->back()->with('error',$e->getMessage());
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $imagen = CentroImage::findOrFail($id);
        $imagen->url = $request->url;
        $imagen->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $image = CentroImage::findOrFail($id);
            Storage::delete($image->image);

            $image->delete();

            return redirect()->back()->with('success','La imagen se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
