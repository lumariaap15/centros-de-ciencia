<?php

namespace App\Http\Controllers;

use App\Aliado;
use App\Centro;
use App\CentrosCategoria;
use App\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    private $centros;
    private $tipos;

    public function __construct()
    {
        $this->centros = Centro::query()->orderBy('nombre')->get();
        $this->tipos = CentrosCategoria::query()->where('categoria_id',null)->get();
    }

    public function inicio()
    {
        $activeMenu = 0;
        return view('front.inicio',compact('activeMenu'));
    }

    public function centros()
    {
        $centros = $this->centros;
        $tipos = $this->tipos;
        $activeMenu = 1;
        return view('front.centros',compact('centros','tipos','activeMenu'));
    }

    public function mapa()
    {
        $centros = $this->centros;
        $tipos = $this->tipos;
        $activeMenu = 2;
        return view('front.mapa',compact('centros','tipos','activeMenu'));
    }

    public function proyectos()
    {
        $centros = $this->centros;
        $tipos = $this->tipos;
        $activeMenu = 3;
        return view('front.proyectos',compact('centros','tipos','activeMenu'));
    }

    public function proyectosPartial(Request $request)
    {
        //dd($request->centros);
        if($request->centros === 'all'){
            $centros = Centro::query()->pluck('id');
        }else{
            $centros = $request->centros;
        }
        $proyectos = Proyecto::query()->with('subtipo','centro')
            ->whereIn('centro_id',$centros)
            ->get();
        return view('front.proyectos-partial',compact('proyectos'));
    }

    public function aliados()
    {
        $centros = $this->centros;
        $tipos = $this->tipos;
        $activeMenu = 4;
        return view('front.aliados',compact('centros','tipos','activeMenu'));
    }

    public function aliadosPartial(Request $request)
    {
        if($request->centros === 'all'){
            $centros = Centro::query()->pluck('id');
        }else{
            $centros = $request->centros;
        }
        //de la tabla many to many buscamos los ids de los aliados que se relacionen con los centros
        $aliadoArr = DB::table('aliado_centro')
            ->whereIn('centro_id',$centros)
            ->pluck('aliado_id');
        $aliados = Aliado::query()->whereIn('id',$aliadoArr)->get();
        return view('front.aliados-partial',compact('aliados'));
    }

    public function inputCentros(Request $request)
    {
        $tipoId = $request->tipo;
        $subtipoId = $request->subtipo;
        $busqueda = $request->input;

        if($subtipoId === null) {
            if($tipoId === null) {
                $centros = Centro::query()->with('subtipo')->where('nombre', 'like', '%' . $busqueda . '%')->get();
            }else{
                $centros = Centro::query()->with('subtipo')->where('nombre', 'like', '%' . $busqueda . '%')
                    ->whereHas('subtipo',function ($query) use ($tipoId){
                        $query->where('categoria_id',$tipoId);
                    })->get();
            }
        }else{
            $centros = Centro::query()->with('subtipo')->where('nombre', 'like', '%' . $busqueda . '%')
                ->where('categoria_id',$subtipoId)->get();
        }
        return $centros;
    }

    public function detallesCentro($centroId)
    {
        $centro = Centro::query()->with('proyectos','servicios','entradas','aliados','horarios','imagenes')->findOrFail($centroId);
        return view('front.detalle-centro',compact('centro'));
    }

    public function detallesProyecto($proyectoId)
    {
        $proyecto = Proyecto::query()->with('centro')->findOrFail($proyectoId);
        return view('front.detalle-proyecto',compact('proyecto'));
    }

}
