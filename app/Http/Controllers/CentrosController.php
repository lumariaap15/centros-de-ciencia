<?php

namespace App\Http\Controllers;

use App\Aliado;
use App\Centro;
use App\CentrosCategoria;
use App\Departamento;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\CentroRequest;
use App\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CentrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centros = Centro::with('subtipo','municipio')->get();
        return view('admin.centros.index', compact('centros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::query()->orderBy('descripcion')->get();
        $categoriasPadre = CentrosCategoria::query()->where('categoria_id',null)->get();
        $categoriasHijas = CentrosCategoria::query()->where('categoria_id', !null)->get();

        return view('admin.centros.create',compact('categoriasPadre','categoriasHijas','departamentos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CentroRequest $request)
    {
        try{
            $centro = new Centro();
            $centro->fill($request->all());
            if(intval($request->departamento_codigo)===8 || intval($request->departamento_codigo)===5){
                $centro->departamento_codigo = '0'.$request->departamento_codigo;
            }
            if($request->has('audio')){
                $audio = Storage::put('public',$request->audio);
                $centro->audio = $audio;

            }
            $centro->save();
            return Response::responseSuccess('Bien! El centro se creó correctamente',CodesResponse::CODE_OK,$centro);

        }catch (\Exception $e){
            return Response::responseError($e->getMessage(),CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centro = Centro::query()->findOrFail($id);
        $departamentos = Departamento::query()->orderBy('descripcion')->get();
        $municipios = Municipio::query()->where('departamento_codigo',$centro->departamento_codigo)->get();
        $categoriasPadre = CentrosCategoria::query()->where('categoria_id',null)->get();
        $categoriasHijas = CentrosCategoria::query()->where('categoria_id',$centro->subtipo->categoria_id)->get();
        return view('admin.centros.edit',compact('centro','categoriasPadre','categoriasHijas','departamentos','municipios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CentroRequest $request, $id)
    {

        try{
            $centro = Centro::query()->findOrFail($id);
            $centro->fill($request->all());
            if(intval($request->departamento_codigo)===8 || intval($request->departamento_codigo)===5){
                $centro->departamento_codigo = '0'.$request->departamento_codigo;
            }
            if($request->has('audio')){

                Storage::delete($centro->audio);
                $centro->audio = null;
                $audio = Storage::put('public',$request->audio);
                $centro->audio = $audio;

            }
            $centro->save();
            return Response::responseSuccess('Bien! El centro se actualizó correctamente',CodesResponse::CODE_OK,$centro);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(),CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $centro = Centro::findOrFail($id);
            $centro->delete();
            return redirect()->back()->with('success','El centro se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
    public function destroyAudio($id){
        try{
            $centro = Centro::findOrFail($id);
            Storage::delete($centro->audio);
            $centro->audio = null;
            $centro->save();
            return redirect()->back()->with('Se eliminó correctamente el audio');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function getSubtipos($tipoId)
    {
        $subtipos = CentrosCategoria::query()->where('categoria_id',$tipoId)->get();
        return $subtipos;
    }
    public function getMunicipios($departamentoId)
    {
        if(intval($departamentoId)===8 || intval($departamentoId)===5){
            $departamentoId = '0'.$departamentoId;
        }
        $subtipos = Municipio::query()->where('departamento_codigo',$departamentoId)->orderBy('descripcion')->get();
        return $subtipos;
    }
    public function indexAliados($centroId)
    {
        $centroNombre = Centro::findOrFail($centroId)->nombre;
        $aliados = Centro::findOrFail($centroId)->aliados;
        return view('admin.centros.aliados', compact('centroId','centroNombre','aliados'));
    }
    public function getAliados($centroId){
       $centro = Centro::query()->findOrFail($centroId);
        $aliados  = Aliado::query()->whereNotIn('id',$centro->aliados->pluck('id'))->get();
      return $aliados;

    }
    public function asociarAliados($centroId,Request $request){
        $centro = Centro::findOrFail($centroId);
        $centro->aliados()->attach($request->aliado_id);
        $validator = Validator::make($request->all(), [
            'aliado_id' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error','El campo Aliado es requerido');
        }
        return redirect()->back()->with('success','El aliado se agregó correctamente');
    }
    public function borrarAliados($centroId, $id){
        $centro = Centro::findOrFail($centroId);
        $centro->aliados()->detach($id);
        return redirect()->back()->with('success','El aliado se eliminó correctamente');
    }
}
