<?php

namespace App\Http\Controllers;

use App\Centro;
use App\CentrosHorario;
use App\Http\Requests\HorariosRequest;
use Illuminate\Http\Request;

class HorariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($centroId)
    {
        $horarios = CentrosHorario::query()->where('centro_id',$centroId)->get();
        $centroNombre = Centro::findOrFail($centroId)->nombre;
        return view('admin.horarios',compact('horarios','centroNombre','centroId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HorariosRequest $request)
    {

        try{
            $horario = new CentrosHorario();
            $horario->fill($request->all());
            $horario->centro_id = $request->centroId;
            $horario->save();
            return redirect()->back()->with('success','El horario se agregó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HorariosRequest $request, $id)
    {
        try{
            $horario = CentrosHorario::findOrFail($id);
            $horario->fill($request->all());
            $horario->horario = $request -> horario_edit;
            $horario->save();
            return redirect()->back()->with('success','El horario se actualizó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $horario = CentrosHorario::findOrFail($id);

            $horario->delete();
            return redirect()->back()->with('success','El horario se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
