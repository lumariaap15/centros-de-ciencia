<?php

namespace App\Http\Controllers;

use App\Centro;
use App\CentrosCategoria;
use App\CentrosEntrada;
use App\Http\Requests\EntradasRequest;
use Illuminate\Http\Request;

class EntradasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($centroId)
    {
        $entradas = CentrosEntrada::query()->where('centro_id',$centroId)->get();
        $centroNombre = Centro::findOrFail($centroId)->nombre;


        return view('admin.entradas',compact('entradas','centroNombre','centroId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntradasRequest $request)
    {

        try{
            $entrada = new CentrosEntrada();
            $entrada->fill($request->all());
            $sinPuntos = str_replace('.','',$request->valor);
            $entrada->valor = $sinPuntos;
            $entrada->centro_id = $request->centroId;
            $entrada->save();
            return redirect()->back()->with('success','La entrada se agregó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EntradasRequest $request, $id)
    {
        try{
            $entrada = CentrosEntrada::findOrFail($id);
            $entrada->valor = $request -> valor_edit;
            $entrada->nombre = $request -> nombre_edit;
            $entrada->save();
            return redirect()->back()->with('success','La entrada se actualizó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $entrada = CentrosEntrada::findOrFail($id);

            $entrada->delete();
            return redirect()->back()->with('success','La entrada se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
