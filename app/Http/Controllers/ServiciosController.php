<?php

namespace App\Http\Controllers;

use App\Centro;
use App\CentrosServicio;
use App\Http\Requests\ServiciosRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($centroId)
    {
        $servicios = CentrosServicio::query()->where('centro_id',$centroId)->get();
        $centroNombre = Centro::findOrFail($centroId)->nombre;

        return view('admin.servicios',compact('servicios','centroNombre','centroId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiciosRequest $request)
    {
        try{
            /*
            $v = Validator::make($request->all(),[
                'descripcion'=>'required'
            ]);
            if($v->fails()){
                return redirect()->back()->withErrors($v);
            }
            */
            $servicio = new CentrosServicio();
            $servicio->fill($request->all());
            $servicio->centro_id = $request->centroId;
            $servicio->save();
            return redirect()->back()->with('success','El servicio se agregó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiciosRequest $request, $id)
    {

        try{
            $servicio = CentrosServicio::findOrFail($id);
            $servicio->descripcion = $request->descripcion_edit;
            $servicio->save();
            return redirect()->back()->with('success','El servicio se actualizó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $servicio = CentrosServicio::findOrFail($id);

            $servicio->delete();
            return redirect()->back()->with('success','El servicio se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
