<?php

namespace App\Http\Controllers;

use App\Aliado;
use App\Http\Requests\AliadosRequest;
use Illuminate\Http\Request;

class AliadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aliados = Aliado::query()->get();

        return view('admin.aliados',compact('aliados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AliadosRequest $request)
    {
        try{
            $aliado = new Aliado();
            $aliado->fill($request->all());
            $aliado->save();
            return redirect()->back()->with('success','El aliado se agregó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AliadosRequest $request, $id)
    {
        try{
            $aliado= Aliado::findOrFail($id);
            $aliado->nombre = $request -> nombre_edit;
            $aliado->url = $request -> url_edit;
            $aliado->save();
            return redirect()->back()->with('success','El aliado se actualizó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $aliado = Aliado::findOrFail($id);

            $aliado->delete();
            return redirect()->back()->with('success','EL aliado se eliminó correctamente.');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
