<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AliadosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method() === 'POST') {
            return [
                'nombre' => 'required|unique:aliados,nombre',
                'url' => 'required',
            ];
        }elseif ($this->method() === 'PUT'){
            return [
                'nombre_edit' => 'required|unique:aliados,nombre,'.$this->route('aliado'),
                'url_edit' => 'required',
            ];
        }

    }

    public function messages()
    {
        return [
            'nombre_edit.required' => 'El campo nombre es requerido.',
            'nombre_edit.unique' => 'El nombre ya esta en uso.',
            'nombre.required' => 'El campo nombre es requerido.',
            'url_edit.required' => 'El campo url es requerido.',
            'url.required' => 'El campo url es requerido',
        ];
    }
}
