<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HorariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() === 'POST') {
            return [
                'horario' => 'required'
            ];
        }elseif ($this->method() === 'PUT'){
            return [
                'horario_edit' => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'horario_edit.required' => 'El campo horario es requerido.',
            'horario.required' => 'El campo horariox es requerido.',
        ];
    }
}
