<?php

namespace App\Http\Requests;

use App\Globals\CodesResponse;
use App\Globals\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CentroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'direccion' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'correo' => 'email|nullable',
            'celular' => 'max:10',
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'El campo nombre es requerido.',
            'direccion.required' => 'El campo dirección es requerido.',
            'latitud.required' => 'El campo latitud es requerido.',
            'longitud.required' => 'El campo longitud es requerido.',
            'correo.email' => 'El campo correo no es un correo válido',
            'size' => 'El campo celular es max 10 digitos',


        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors()->first(), 422));
    }
}
