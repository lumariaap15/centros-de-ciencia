<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiciosRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() === 'POST') {
            return [
                'descripcion' => 'required'
            ];
        }elseif ($this->method() === 'PUT'){
            return [
                'descripcion_edit' => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'descripcion_edit.required' => 'El campo descripción es requerido.',
            'descripcion.required' => 'El campo descripción es requerido.',
        ];
    }
}
