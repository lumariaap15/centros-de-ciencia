<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntradasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() === 'POST') {
            return [
                'nombre' => 'required',
                'valor' => 'required|numeric|min:0',
            ];
        }elseif ($this->method() === 'PUT'){
            return [
                'nombre_edit' => 'required',
                'valor_edit' => 'required|numeric|min:0',
            ];
        }
    }
    public function messages()
    {
        return [
            'nombre_edit.required' => 'El campo nombre es requerido.',
            'nombre.required' => 'El campo nombre es requerido.',
            'valor_edit.required' => 'El campo valor es requerido y numerico.',
            'valor.required' => 'El campo valor es requerido y numerico.',
        ];
    }
}
