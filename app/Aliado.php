<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aliado extends Model
{
    protected $fillable=[
        'nombre',
        'url'
    ];

    public function centros(){
        $this->belongsToMany(Centro::class);
    }
}
