<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centro extends Model
{
    protected $fillable = [
        "nombre",
        "categoria_id",
        "owner",
        "descripcion",
        "direccion",
        "latitud",
        "longitud",
        "pagina_web",
        "correo",
        "telefono",
        "celular",
        "fecha_creacion",
        "departamento_codigo",
        "municipio_codigo",
    ];

    public function subtipo()
    {
        return $this->hasOne(CentrosCategoria::class,'id','categoria_id');
    }

    public function departamento()
    {
        return $this->hasOne(Departamento::class,'codigo','departamento_codigo');
    }

    public function municipio()
    {
        return $this->hasOne(Municipio::class,'codigo','municipio_codigo');
    }

    public function proyectos()
    {
        return $this->hasMany(Proyecto::class);
    }

    public function aliados(){
        return $this->belongsToMany(Aliado::class);
    }

    public function servicios()
    {
        return $this->hasMany(CentrosServicio::class);
    }

    public function entradas()
    {
        return $this->hasMany(CentrosEntrada::class);
    }

    public function horarios()
    {
        return $this->hasMany(CentrosHorario::class);
    }

    public function imagenes()
    {
        return $this->hasMany(CentroImage::class);
    }


}
