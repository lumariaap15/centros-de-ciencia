<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentrosCategoria extends Model
{
    CONST BIOESPACIOS_ID = 1;
    CONST CIENCIAS_ID = 2;
    CONST CIUDADANOS_ID = 3;
    CONST MIXTOS_ID = 4;

    public function padre()
    {
        return $this->hasOne(CentrosCategoria::class,'id','categoria_id');
    }
}
