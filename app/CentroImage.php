<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroImage extends Model
{
    protected $fillable=[
      'image',
    ];
}
